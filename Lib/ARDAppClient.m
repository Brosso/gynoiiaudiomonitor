/*
 * libjingle
 * Copyright 2014, Google Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. The name of the author may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "ARDAppClient.h"

#import <AVFoundation/AVFoundation.h>

#import "ARDMessageResponse.h"
#import "ARDRegisterResponse.h"
#import "ARDSignalingMessage.h"
#import "ARDUtilities.h"
#import "ARDWebSocketChannel.h"
#import "RTCICECandidate+JSON.h"
#import "RTCICEServer+JSON.h"
#import "RTCMediaConstraints.h"
#import "RTCMediaStream.h"
#import "RTCPair.h"
#import "RTCPeerConnection.h"
#import "RTCPeerConnectionDelegate.h"
#import "RTCPeerConnectionFactory.h"
#import "RTCSessionDescription+JSON.h"
#import "RTCSessionDescriptionDelegate.h"
#import "RTCVideoCapturer.h"
#import "RTCVideoTrack.h"

// TODO(tkchin): move these to a configuration object.
static NSString *kARDRoomServerHostUrl =
    @"https://apprtc.appspot.com";
static NSString *kARDRoomServerRegisterFormat =
    @"%@/join/%@";
static NSString *kARDRoomServerMessageFormat =
    @"%@/message/%@/%@";
static NSString *kARDRoomServerByeFormat =
    @"%@/leave/%@/%@";

static NSString *kARDDefaultSTUNServerUrl =
    @"stun:stun.l.google.com:19302";
// TODO(tkchin): figure out a better username for CEOD statistics.
static NSString *kARDTurnRequestUrl =
    @"https://computeengineondemand.appspot.com"
    @"/turn?username=iapprtc&key=4080218913";

static NSString *kARDAppClientErrorDomain = @"ARDAppClient";
static NSInteger kARDAppClientErrorUnknown = -1;
static NSInteger kARDAppClientErrorRoomFull = -2;
static NSInteger kARDAppClientErrorCreateSDP = -3;
static NSInteger kARDAppClientErrorSetSDP = -4;
static NSInteger kARDAppClientErrorNetwork = -5;
static NSInteger kARDAppClientErrorInvalidClient = -6;
static NSInteger kARDAppClientErrorInvalidRoom = -7;

@interface ARDAppClient () <ARDWebSocketChannelDelegate,
    RTCPeerConnectionDelegate, RTCSessionDescriptionDelegate>
@property(nonatomic, strong) ARDWebSocketChannel *channel;
@property(nonatomic, strong) RTCPeerConnection *peerConnection;
@property(nonatomic, strong) RTCPeerConnectionFactory *factory;
@property(nonatomic, strong) NSMutableArray *messageQueue;

@property(nonatomic, assign) BOOL isTurnComplete;
@property(nonatomic, assign) BOOL hasReceivedSdp;
@property(nonatomic, readonly) BOOL isRegisteredWithRoomServer;

@property(nonatomic, strong) NSString *roomId;
@property(nonatomic, strong) NSString *clientId;
@property(nonatomic, assign) BOOL isInitiator;
@property(nonatomic, assign) BOOL audioEnabled;
@property(nonatomic, assign) BOOL videoEnabled;
@property(nonatomic, assign) BOOL ledEnabled;
@property(nonatomic, strong) NSMutableArray *iceServers;
@property(nonatomic, strong) NSURL *webSocketURL;
@property(nonatomic, strong) NSURL *webSocketRestURL;
@end

@implementation ARDAppClient
{
    SocketManager *manager;
    BOOL shouldRetry;
    AVAudioPlayer *myMusicPlayer;
    NSTimer *timeOut;
}

@synthesize delegate = _delegate;
@synthesize state = _state;
@synthesize serverHostUrl = _serverHostUrl;
@synthesize channel = _channel;
@synthesize peerConnection = _peerConnection;
@synthesize factory = _factory;
@synthesize messageQueue = _messageQueue;
@synthesize isTurnComplete = _isTurnComplete;
@synthesize hasReceivedSdp  = _hasReceivedSdp;
@synthesize roomId = _roomId;
@synthesize clientId = _clientId;
@synthesize isInitiator = _isInitiator;
@synthesize iceServers = _iceServers;
@synthesize webSocketURL = _websocketURL;
@synthesize webSocketRestURL = _websocketRestURL;

- (instancetype)initWithDelegate:(id<ARDAppClientDelegate>)delegate {
  if (self = [super init]) {
    _delegate = delegate;
    _factory = [[RTCPeerConnectionFactory alloc] init];
    _messageQueue = [NSMutableArray array];
    _iceServers = [NSMutableArray arrayWithObject:[self defaultSTUNServer]];
    _serverHostUrl = kARDRoomServerHostUrl;
      
       manager = [SocketManager sharedInstance];
//#ifdef DEBUG
//      NSLog(@"last peer :%@",[manager getLastPeer]);
//#endif
//      if (nil == [manager getLastPeer]) {
//          [manager broadcastLANWithMessage:nil];
//      }
      
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(orientationChanged:)
                                                   name:@"UIDeviceOrientationDidChangeNotification"
                                                 object:nil];
      
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveLANMessage:) name:@"LAN_CAPTURED" object:nil];
      
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive) name:UIApplicationDidEnterBackgroundNotification object:nil];
      
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBecomeActive) name:UIApplicationWillEnterForegroundNotification object:nil];
      
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hadleCaptureError:) name:AVCaptureSessionRuntimeErrorNotification object:nil];
      
       [manager listenLAN];
  }
  return self;
}

- (void)appWillResignActive {
    if (_isInitiator) {
        [manager broadcastLANWithMessage:@"GYNOII_VID_OFF"];
    }
}

- (void)appWillBecomeActive {
    if (_isInitiator) {
        [manager broadcastLANWithMessage:@"GYNOII_VID_ON"];
    }
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LAN_CAPTURED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureSessionRuntimeErrorNotification object:nil];
    
  [self disconnect];
    
//    NSLog(@"appClient dealloc");
}

- (void)orientationChanged:(NSNotification *)notification {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
//    // Disable this
//    if (UIDeviceOrientationIsLandscape(orientation) || UIDeviceOrientationIsPortrait(orientation)) {
//        //Remove current video track
//        RTCMediaStream *localStream = _peerConnection.localStreams[0];
//        
//        if (0 == [localStream.videoTracks count]) {
//            return;
//        }
//        
//        [localStream removeVideoTrack:localStream.videoTracks[0]];
//        
//        RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];
//        if (localVideoTrack) {
//            [localStream addVideoTrack:localVideoTrack];
//            [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
//        }
//        [_peerConnection removeStream:localStream];
//        [_peerConnection addStream:localStream];
//    }
}

- (void)hadleCaptureError:(NSNotification *)notification
{
//    AVCaptureSession *session = [notification object];
//    if ([session.inputs count]) {
//        AVCaptureInput* input = [session.inputs objectAtIndex:0];
//        [session removeInput:input];
//    }
//    if ([session.outputs count]) {
//        AVCaptureVideoDataOutput* output = [session.outputs objectAtIndex:0];
//        [session removeOutput:output];
//    }
//    
//    [session stopRunning];
    
    [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Device is busy, please try again later", nil)];

//    [self resetMonitor];
}


- (void)setState:(ARDAppClientState)state {
  if (_state == state) {
    return;
  }
  _state = state;
  [_delegate appClient:self didChangeState:_state];
}

- (void)setInitiator:(BOOL)isInitiator {
    self.isInitiator = isInitiator;
}

- (void)connectToRoomWithId:(NSString *)roomId
                    options:(NSDictionary *)options {
//  NSParameterAssert(roomId.length);
  NSParameterAssert(_state == kARDAppClientStateDisconnected);
  self.state = kARDAppClientStateConnecting;

    [self startSignalingInLan];
    

//  // Request TURN.
//  __weak ARDAppClient *weakSelf = self;
//  NSURL *turnRequestURL = [NSURL URLWithString:kARDTurnRequestUrl];
//  [self requestTURNServersWithURL:turnRequestURL
//                completionHandler:^(NSArray *turnServers) {
//    ARDAppClient *strongSelf = weakSelf;
//    [strongSelf.iceServers addObjectsFromArray:turnServers];
//    strongSelf.isTurnComplete = YES;
//    [strongSelf startSignalingIfReady];
//  }];
//
//  // Register with room server.
//  [self registerWithRoomServerForRoomId:roomId
//                      completionHandler:^(ARDRegisterResponse *response) {
//    ARDAppClient *strongSelf = weakSelf;
//    if (!response || response.result != kARDRegisterResultTypeSuccess) {
//      NSLog(@"Failed to register with room server. Result:%d",
//          (int)response.result);
//      [strongSelf disconnect];
//      NSDictionary *userInfo = @{
//        NSLocalizedDescriptionKey: @"Room is full.",
//      };
//      NSError *error =
//          [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                     code:kARDAppClientErrorRoomFull
//                                 userInfo:userInfo];
//      [strongSelf.delegate appClient:strongSelf didError:error];
//      return;
//    }
//    NSLog(@"Registered with room server.");
//    strongSelf.roomId = response.roomId;
//    strongSelf.clientId = response.clientId;
//    strongSelf.isInitiator = response.isInitiator;
//    for (ARDSignalingMessage *message in response.messages) {
//      if (message.type == kARDSignalingMessageTypeOffer ||
//          message.type == kARDSignalingMessageTypeAnswer) {
//        strongSelf.hasReceivedSdp = YES;
//        [strongSelf.messageQueue insertObject:message atIndex:0];
//      } else {
//        [strongSelf.messageQueue addObject:message];
//      }
//    }
//    strongSelf.webSocketURL = response.webSocketURL;
//    strongSelf.webSocketRestURL = response.webSocketRestURL;
//    [strongSelf registerWithColliderIfReady];
//    [strongSelf startSignalingIfReady];
//  }];
}

- (void)disconnect {
  if (_state == kARDAppClientStateDisconnected) {
    return;
  }
//  if (self.isRegisteredWithRoomServer) {
//    [self unregisterWithRoomServer];
//  }
  if (_channel) {
    if (_channel.state == kARDWebSocketChannelStateRegistered) {
      // Tell the other client we're hanging up.
      ARDByeMessage *byeMessage = [[ARDByeMessage alloc] init];
      NSData *byeData = [byeMessage JSONData];
      [_channel sendData:byeData];
    }
    // Disconnect from collider.
    _channel = nil;
  }
  _clientId = nil;
  _roomId = nil;
//  _isInitiator = NO;
  _hasReceivedSdp = NO;
  _messageQueue = [NSMutableArray array];
  _peerConnection = nil;
  self.state = kARDAppClientStateDisconnected;
    
    [manager stopListenLAN];
    shouldRetry = NO;
}

- (void)toggleVideoTrack
{
    _videoEnabled = !_videoEnabled;
//    NSLog(@"_videoEnabled = %d",_videoEnabled);
    [self toggleStreamTrack];
}

- (void)toggleAudioTrack
{
    _audioEnabled = !_audioEnabled;
//    NSLog(@"_audioEnabled = %d",_audioEnabled);
    [self toggleStreamTrack];
}

- (BOOL)audioState
{
    return _audioEnabled;
}

- (void)toggleLEDOn
{
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setObject:@"ledOn" forKey:@"type"];
    
    [self sendJsonCommand:jsonDict];
}

- (BOOL)ledState
{
    return _ledEnabled;
}

- (void)toggleLED
{
    _ledEnabled = !_ledEnabled;
    for (AVCaptureDevice *captureDevice in
         [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
        if (captureDevice.position == AVCaptureDevicePositionBack) { //AVCaptureDevicePositionFront
            if ([captureDevice hasTorch]) {
                [captureDevice lockForConfiguration:nil];
                [captureDevice setTorchMode:(_ledEnabled)?AVCaptureTorchModeOn:AVCaptureTorchModeOff];
                [captureDevice unlockForConfiguration];
            }
            break;
        }
    }
}

- (void)getMusicList
{
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setObject:@"getMusicList" forKey:@"type"];
    
    [self sendJsonCommand:jsonDict];
}

- (void)playMusic:(NSString*)title
{
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setObject:@"musicPlay" forKey:@"type"];
    [jsonDict setObject:title forKey:@"name"];
    
    [self sendJsonCommand:jsonDict];
}

- (void)stopMusic
{
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setObject:@"musicStop" forKey:@"type"];
    
    [self sendJsonCommand:jsonDict];
}


#pragma mark - NSNotification
- (void)didReceiveLANMessage:(NSNotification*) notification
{
    NSString *message = [notification object];
#ifdef DEBUG
    NSLog(@"receive message \n%@",message);
#endif
    ARDSignalingMessage *msg = [ARDSessionDescriptionMessage messageFromJSONString:message];
    
    if (nil == msg) { // could be GYNOII_PING / PONG / or other control message (LED, front / back camera)
        // new style custom message
        NSData *messageData = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:messageData options:0 error:nil];
        NSString *command = [jsonDict objectForKey:@"type"];
        
        // filter out GYNOII_PONG
        if ([message isEqualToString:@"GYNOII_PONG"] || [command isEqualToString:@"pong"]) {
            return;
        }
        
        if (!_isInitiator) { // monitor
            // old style
            if ([message isEqualToString:@"GYNOII_VID_ON"] || [message isEqualToString:@"GYNOII_VID_OFF"]) {
                [self toggleVideoTrack];
            }
            if ([command isEqualToString:@"ledOn"]) {
                [self toggleLED];
            }
//            else if ([command isEqualToString:@"ledOff"]) {
//                [self toggleLED];
//            }
            else if ([command isEqualToString:@"musicPlay"]) {
                NSString *title = [jsonDict objectForKey:@"name"];
                
                NSError *error;
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *path = [documentsDirectory stringByAppendingPathComponent:@"playlist.plist"];
                NSString *bundle = [[NSBundle mainBundle] pathForResource:@"playlist" ofType:@"plist"];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if (![fileManager fileExistsAtPath: path])
                {
                    [fileManager copyItemAtPath:bundle toPath: path error:&error];
                }
                
                NSArray *playList = [[NSArray alloc] initWithContentsOfFile:path];
                NSString *urlString;
                NSURL *url;
				Boolean native = false;
                
                for (NSDictionary *dict in playList)
                {
                    if ([[dict objectForKey:@"name"] isEqualToString:title]) {
                        if ([[dict objectForKey:@"url"] hasPrefix:@"ipod-library"]) {
                            urlString = [dict objectForKey:@"url"];
                            url = [NSURL URLWithString:urlString];
							native = false;
                        }
                        else {
							native = true;
                            urlString = [[NSBundle mainBundle] pathForResource:[[dict objectForKey:@"url"] stringByDeletingPathExtension] ofType:[[dict objectForKey:@"url"] pathExtension]];
                            url = [NSURL fileURLWithPath:urlString];
                        }
                    }
                }
                
//                NSLog(@"prepare to play urlString :%@",urlString);
                
                if (nil != url) {
                    if (myMusicPlayer) {
                        [myMusicPlayer stop];
                        myMusicPlayer = nil;
                    }
                    
                    // need to temporarily disable remote player
                    
                    // Set audio category
                    AVAudioSession *session = [AVAudioSession sharedInstance];
                    
                    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
                    [session setMode:AVAudioSessionModeMoviePlayback error:nil];
                    
                    // Override audio output to speaker
                    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
                    [session setActive:YES error:nil];
                    
                    // Init music player
                    myMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
                    [myMusicPlayer setDelegate:self];
                    [myMusicPlayer setVolume:1.0];
					
					if (native) {
						myMusicPlayer.numberOfLoops = -1;
					}
                    [myMusicPlayer prepareToPlay];
                    [myMusicPlayer play];

                }
                
            }
            else if ([command isEqualToString:@"musicStop"]) {
                
                // Set audio category back
                AVAudioSession *session = [AVAudioSession sharedInstance];
                
                [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
                [session setMode:AVAudioSessionModeVideoChat error:nil];
                
                if (myMusicPlayer) {
                    [myMusicPlayer stop];
                    myMusicPlayer = nil;
                }
            }
            else if ([command isEqualToString:@"getMusicList"]) {

                NSMutableDictionary *aJsonDict = [[NSMutableDictionary alloc] init];
                [aJsonDict setObject:@"musicList" forKey:@"type"];
                
                NSError *error;
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *path = [documentsDirectory stringByAppendingPathComponent:@"playlist.plist"];
                NSString *bundle = [[NSBundle mainBundle] pathForResource:@"playlist" ofType:@"plist"];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if (![fileManager fileExistsAtPath: path])
                {
                    [fileManager copyItemAtPath:bundle toPath: path error:&error];
                }
                
                NSArray *playList = [[NSArray alloc] initWithContentsOfFile:path];
                
                NSMutableArray *finalSongList = [[NSMutableArray alloc] init];
                for (NSDictionary *dict in playList) {
                    [finalSongList addObject:[dict objectForKey:@"name"]];
                }
                
                [aJsonDict setObject:finalSongList forKey:@"list"];
                
                [self sendJsonCommand:aJsonDict];
            }
        }
        
        else { // viewer
            NSData *messageData = [message dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:messageData options:0 error:nil];
            NSString *command = [jsonDict objectForKey:@"type"];
            
            if ([command isEqualToString:@"musicList"]) {
                NSArray *musicList = [jsonDict objectForKey:@"list"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GOT_MUSIC_LIST" object:musicList];
            }
            else if ([command isEqualToString:@"musicPlay"]) {
                
            }
        }
        
        return;
    }
    
    switch (msg.type) {
        case kARDSignalingMessageTypeOffer:
        case kARDSignalingMessageTypeAnswer:
            if (_hasReceivedSdp) {
                break;
            }
            _hasReceivedSdp = YES;
            [_messageQueue insertObject:msg atIndex:0];
            break;
        case kARDSignalingMessageTypeCandidate:
            [_messageQueue addObject:msg];
            break;
        case kARDSignalingMessageTypeBye:
            [self processSignalingMessage:msg];
            return;
    }
    [self drainMessageQueueIfReady];
}


#pragma mark - ARDWebSocketChannelDelegate

- (void)channel:(ARDWebSocketChannel *)channel
    didReceiveMessage:(ARDSignalingMessage *)message {
  switch (message.type) {
    case kARDSignalingMessageTypeOffer:
    case kARDSignalingMessageTypeAnswer:
      _hasReceivedSdp = YES;
      [_messageQueue insertObject:message atIndex:0];
      break;
    case kARDSignalingMessageTypeCandidate:
      [_messageQueue addObject:message];
      break;
    case kARDSignalingMessageTypeBye:
      [self processSignalingMessage:message];
      return;
  }
  [self drainMessageQueueIfReady];
}

- (void)channel:(ARDWebSocketChannel *)channel
    didChangeState:(ARDWebSocketChannelState)state {
  switch (state) {
    case kARDWebSocketChannelStateOpen:
      break;
    case kARDWebSocketChannelStateRegistered:
      break;
    case kARDWebSocketChannelStateClosed:
    case kARDWebSocketChannelStateError:
      // TODO(tkchin): reconnection scenarios. Right now we just disconnect
      // completely if the websocket connection fails.
      [self disconnect];
      break;
  }
}

#pragma mark - RTCPeerConnectionDelegate

- (void)peerConnection:(RTCPeerConnection *)peerConnection
    signalingStateChanged:(RTCSignalingState)stateChanged {
#ifdef DEBUG
  NSLog(@"Signaling state changed: %d", stateChanged);
#endif
    
    // Monitor needs to add timer to prevent false connect
    if (!_isInitiator) { // monitor
        if (RTCSignalingHaveRemoteOffer == stateChanged) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Handletimeout
//                NSLog(@"Start timeout");
                timeOut = [NSTimer scheduledTimerWithTimeInterval:15.0f target:self selector:@selector(resetMonitor) userInfo:nil repeats:NO];
            });
        }
    }
}

- (void)resetMonitor
{
    // stop timeout
    if ([timeOut isValid]) {
#ifdef DEBUG
        NSLog(@"Stop timeout");
#endif
        [timeOut invalidate];
        timeOut = nil;
    }
    
    NSLog(@"Timeout!!! Reset monitor");
    [self disconnect];
    [manager listenLAN];
    [self startSignalingInLan];
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
           addedStream:(RTCMediaStream *)stream {
  dispatch_async(dispatch_get_main_queue(), ^{
#ifdef DEBUG
    NSLog(@"Received %lu video tracks and %lu audio tracks",
        (unsigned long)stream.videoTracks.count,
        (unsigned long)stream.audioTracks.count);
#endif
    if (stream.videoTracks.count) {
      RTCVideoTrack *videoTrack = stream.videoTracks[0];
      [_delegate appClient:self didReceiveRemoteVideoTrack:videoTrack];
    }
  });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
        removedStream:(RTCMediaStream *)stream {
//  NSLog(@"Stream was removed.");
}

- (void)peerConnectionOnRenegotiationNeeded:
    (RTCPeerConnection *)peerConnection {
//  NSLog(@"WARNING: Renegotiation needed but unimplemented.");
    
    
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
    iceConnectionChanged:(RTCICEConnectionState)newState {
#ifdef DEBUG
  NSLog(@"ICE state changed: %d", newState);
#endif
    if (RTCICEConnectionDisconnected == newState) {
#ifdef DEBUG
        NSLog(@"RTCICEConnectionDisconnected, need to reset peerconnection");
#endif
        [self resetMonitor];
    }
    else if (/*RTCICEConnectionCompleted == newState ||*/ RTCICEConnectionConnected == newState) {
#ifdef DEBUG
        NSLog(@"ConnectionConnected");
#endif
        // stop timeout
        if ([timeOut isValid]) {
#ifdef DEBUG
            NSLog(@"Stop timeout");
#endif
            [timeOut invalidate];
            timeOut = nil;
        }
        
        shouldRetry = NO;
        
        if (_isInitiator) {
            [self getMusicList];
        }
        
        // Set audio category
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        
        // Override audio output to speaker
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
        
    }
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
    iceGatheringChanged:(RTCICEGatheringState)newState {
#ifdef DEBUG
  NSLog(@"ICE gathering state changed: %d", newState);
#endif
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
       gotICECandidate:(RTCICECandidate *)candidate {
  dispatch_async(dispatch_get_main_queue(), ^{
    ARDICECandidateMessage *message =
        [[ARDICECandidateMessage alloc] initWithCandidate:candidate];
      
      // send candidate here
      NSString *candidateString = [message description];
//      [manager broadcastLANWithMessage:candidateString];
      NSString *host = [manager getLastPeer];
      
      if (nil == host || [host isEqualToString:@""]) {
          // Update peer list
          [manager broadcastLANWithMessage:nil];
//          host = @"255.255.255.255"; // will fail for sure
          NSLog(@"nil host");
      }
      
      [manager sendMessage:candidateString toHost:host];
      
//    [self sendSignalingMessage:message];
  });
}

- (void)peerConnection:(RTCPeerConnection*)peerConnection
    didOpenDataChannel:(RTCDataChannel*)dataChannel {
}

#pragma mark - RTCSessionDescriptionDelegate

- (void)peerConnection:(RTCPeerConnection *)peerConnection
    didCreateSessionDescription:(RTCSessionDescription *)sdp
                          error:(NSError *)error {
  dispatch_async(dispatch_get_main_queue(), ^{
    if (error) {
#ifdef DEBUG
      NSLog(@"Failed to create session description. Error: %@", error);
#endif
      [self disconnect];
      NSDictionary *userInfo = @{
        NSLocalizedDescriptionKey: @"Failed to create session description.",
      };
      NSError *sdpError =
          [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                     code:kARDAppClientErrorCreateSDP
                                 userInfo:userInfo];
      [_delegate appClient:self didError:sdpError];
      return;
    }
    [_peerConnection setLocalDescriptionWithDelegate:self
                                  sessionDescription:sdp];
    ARDSessionDescriptionMessage *message =
        [[ARDSessionDescriptionMessage alloc] initWithDescription:sdp];
      
      
      // send offer/answer here
      NSString *offerString = [message description];
      NSString *host = [manager getLastPeer];
      
      if (nil == host || [host isEqualToString:@""]) {
          // Update peer list
          [manager broadcastLANWithMessage:nil];
//          host = @"255.255.255.255"; // will fail for sure
          NSLog(@"nil host");
      }
      
      [manager sendMessage:offerString toHost:host];
      
//    [self sendSignalingMessage:message];
  });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
    didSetSessionDescriptionWithError:(NSError *)error {
  dispatch_async(dispatch_get_main_queue(), ^{
    if (error) {
#ifdef DEBUG
      NSLog(@"Failed to set session description. Error: %@", error);
#endif
      [self disconnect];
      NSDictionary *userInfo = @{
        NSLocalizedDescriptionKey: @"Failed to set session description.",
      };
      NSError *sdpError =
          [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                     code:kARDAppClientErrorSetSDP
                                 userInfo:userInfo];
      [_delegate appClient:self didError:sdpError];
      return;
    }
    // If we're answering and we've just set the remote offer we need to create
    // an answer and set the local description.
    if (!_isInitiator && !_peerConnection.localDescription) {
      RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
      [_peerConnection createAnswerWithDelegate:self
                                    constraints:constraints];

    }
  });
}

#pragma mark - AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
#ifdef DEBUG
    NSLog(@"playback completed");
#endif
    // Set audio category
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setMode:AVAudioSessionModeVideoChat error:nil];
    
    // Override audio output to speaker
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    [session setActive:YES error:nil];
}

#pragma mark - Private

//- (BOOL)isRegisteredWithRoomServer {
//  return _clientId.length;
//}

- (void)startSignalingInLan {
    self.state = kARDAppClientStateConnected;
    
    // Create peer connection.
    RTCMediaConstraints *constraints = [self defaultPeerConnectionConstraints];
    _peerConnection = [_factory peerConnectionWithICEServers:nil//_iceServers
                                                 constraints:constraints
                                                    delegate:self];
    
    if (_isInitiator) { // viewer
        _audioEnabled = YES;
        _videoEnabled = NO;
    } else { // camera
        _audioEnabled = YES;
        _videoEnabled = YES;
    }
    
    
    RTCMediaStream *localStream = [self createLocalMediaStream];
    [_peerConnection addStream:localStream];
    
    if (_isInitiator) { // viewer
        shouldRetry = YES;
        [self sendOffer];
    } else { // camera
        [self waitForAnswer];
    }
}


//- (void)startSignalingIfReady {
//  if (!_isTurnComplete || !self.isRegisteredWithRoomServer) {
//    return;
//  }
//  self.state = kARDAppClientStateConnected;
//
//  // Create peer connection.
//  RTCMediaConstraints *constraints = [self defaultPeerConnectionConstraints];
//  _peerConnection = [_factory peerConnectionWithICEServers:_iceServers
//                                               constraints:constraints
//                                                  delegate:self];
//  RTCMediaStream *localStream = [self createLocalMediaStream];
//  [_peerConnection addStream:localStream];
//  if (_isInitiator) {
//    [self sendOffer];
//  } else {
//    [self waitForAnswer];
//  }
//}

- (void)sendOffer {
    // Setup timeout and retry times
    static int retry = 0;
#ifdef DEBUG
    NSLog(@"offer retry times: %d",retry);
#endif
    
    if (!shouldRetry) {
        retry = 0;
        return;
    }
    
    [_peerConnection createOfferWithDelegate:self
                                 constraints:[self defaultOfferConstraints]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (!_peerConnection || !_hasReceivedSdp) {
        if (retry < 3) {
            // Retry after 10 seconds.
            retry += 1;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self sendOffer];
            });
        }
        else {
//            NSLog(@"retry 3 times already");
            retry = 0;
            
            // Post notification and return
            [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:[NSString stringWithFormat:NSLocalizedString(@"Cannot connect.\nPlease make sure both devices are using same username (%@) and joining same WiFi network (%@)", nil), [defaults objectForKey:@"username"], [Utility getCurrentWifiHotSpotName]]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MaximumRetry" object:nil];
        }
        return;
    }
    else if (retry >= 3) {
        retry = 0;
        
        // Post notification and return
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:[NSString stringWithFormat:NSLocalizedString(@"Cannot connect.\nPlease make sure both devices are using same username (%@) and joining same WiFi network (%@)", nil), [defaults objectForKey:@"username"], [Utility getCurrentWifiHotSpotName]]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MaximumRetry" object:nil];
    }
    
    retry = 0;
}

- (void)waitForAnswer {
  [self drainMessageQueueIfReady];
}

- (void)drainMessageQueueIfReady {
    
  if (!_peerConnection || !_hasReceivedSdp) {
    return;
  }
    
  for (ARDSignalingMessage *message in _messageQueue) {
    [self processSignalingMessage:message];
  }
  [_messageQueue removeAllObjects];
}

- (void)processSignalingMessage:(ARDSignalingMessage *)message {
  NSParameterAssert(_peerConnection ||
      message.type == kARDSignalingMessageTypeBye);
  switch (message.type) {
      case kARDSignalingMessageTypeOffer: {
          if (_isInitiator) {
              break;
          }
          
          ARDSessionDescriptionMessage *sdpMessage =
          (ARDSessionDescriptionMessage *)message;
          RTCSessionDescription *description = sdpMessage.sessionDescription;
          [_peerConnection setRemoteDescriptionWithDelegate:self
                                         sessionDescription:description];
          break;
      }
    case kARDSignalingMessageTypeAnswer: {
      ARDSessionDescriptionMessage *sdpMessage =
          (ARDSessionDescriptionMessage *)message;
      RTCSessionDescription *description = sdpMessage.sessionDescription;
      [_peerConnection setRemoteDescriptionWithDelegate:self
                                     sessionDescription:description];
      break;
    }
    case kARDSignalingMessageTypeCandidate: {
      ARDICECandidateMessage *candidateMessage =
          (ARDICECandidateMessage *)message;
      [_peerConnection addICECandidate:candidateMessage.candidate];
      break;
    }
    case kARDSignalingMessageTypeBye:
      // Other client disconnected.
      // TODO(tkchin): support waiting in room for next client. For now just
      // disconnect.
      [self disconnect];
      break;
  }
}

//- (void)sendSignalingMessage:(ARDSignalingMessage *)message {
//  if (_isInitiator) {
//    [self sendSignalingMessageToRoomServer:message completionHandler:nil];
//  } else {
//    [self sendSignalingMessageToCollider:message];
//  }
//}


- (RTCVideoTrack *)createLocalVideoTrack {
    // The iOS simulator doesn't provide any sort of camera capture
    // support or emulation (http://goo.gl/rHAnC1) so don't bother
    // trying to open a local stream.
    // TODO(tkchin): local video capture for OSX. See
    // https://code.google.com/p/webrtc/issues/detail?id=3417.

    RTCVideoTrack *localVideoTrack = nil;
#if !TARGET_IPHONE_SIMULATOR && TARGET_OS_IPHONE

    NSString *cameraID = nil;
    for (AVCaptureDevice *captureDevice in
         [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
        if (captureDevice.position == AVCaptureDevicePositionBack) { //AVCaptureDevicePositionFront
            cameraID = [captureDevice localizedName];
            break;
        }
    }
    NSAssert(cameraID, @"Unable to get the front camera id");
//    CFAbsoluteTime a = CFAbsoluteTimeGetCurrent();
    
    RTCVideoCapturer *capturer = [RTCVideoCapturer capturerWithDeviceName:cameraID];
//    CFAbsoluteTime b = CFAbsoluteTimeGetCurrent();
//    NSLog(@"RTCVideoCapturer cost %f secs",b-a);
    
    RTCMediaConstraints *mediaConstraints = [self defaultMediaStreamConstraints];
    
    RTCVideoSource *videoSource = [_factory videoSourceWithCapturer:capturer constraints:mediaConstraints];
//    CFAbsoluteTime c = CFAbsoluteTimeGetCurrent();
//    NSLog(@"videoSourceWithCapturer cost %f secs",c-b);
    
    localVideoTrack = [_factory videoTrackWithID:@"ARDAMSv0" source:videoSource];
//    CFAbsoluteTime d = CFAbsoluteTimeGetCurrent();
//    NSLog(@"videoTrackWithID cost %f secs\n\n\n",d-c);
#endif
    
    return localVideoTrack;
}

- (RTCMediaStream *)createLocalMediaStream {
    RTCMediaStream* localStream = [_factory mediaStreamWithLabel:@"ARDAMS"];

    if (_videoEnabled) {
        RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];
        if (localVideoTrack) {
            [localStream addVideoTrack:localVideoTrack];
            [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
        }
    }

    if (_audioEnabled) {
        [localStream addAudioTrack:[_factory audioTrackWithID:@"ARDAMSa0"]];
    }
    
    return localStream;
}

- (void)toggleStreamTrack
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *localStreams = _peerConnection.localStreams;
        RTCMediaStream *localStream = nil;
        
        if ([localStreams count]) { // has existed stream
            localStream = _peerConnection.localStreams[0];
            
            // Remove tracks
            if ([localStream.videoTracks count]) {
                [localStream removeVideoTrack:localStream.videoTracks[0]];
            }
            if ([localStream.audioTracks count]) {
                [localStream removeAudioTrack:localStream.audioTracks[0]];
            }
            
            // Add tracks
            if (_videoEnabled) {
                RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];
                if (localVideoTrack) {
                    [localStream addVideoTrack:localVideoTrack];
                    [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
                }
            }
            
            if (_audioEnabled) {
                [localStream addAudioTrack:[_factory audioTrackWithID:@"ARDAMSa0"]];
            }
            
            // Remove current stream
            [_peerConnection removeStream:localStream];
        }
        else {
            // Recreate new stream
            localStream = [self createLocalMediaStream];
        }
        
        // Add stream back to connection
        [_peerConnection addStream:localStream];
    });
}

- (void)sendJsonCommand:(NSDictionary*)jsonDict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:0 error:&error];
    
    NSString *jsonString = nil;
    if (!jsonData) {
//        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    if (nil != [manager getLastPeer]) {
        [manager sendMessage:jsonString toHost:[manager getLastPeer]];
    }
}

//- (void)requestTURNServersWithURL:(NSURL *)requestURL
//    completionHandler:(void (^)(NSArray *turnServers))completionHandler {
//  NSParameterAssert([requestURL absoluteString].length);
//  NSMutableURLRequest *request =
//      [NSMutableURLRequest requestWithURL:requestURL];
//  // We need to set origin because TURN provider whitelists requests based on
//  // origin.
//  [request addValue:@"Mozilla/5.0" forHTTPHeaderField:@"user-agent"];
//  [request addValue:self.serverHostUrl forHTTPHeaderField:@"origin"];
//  [NSURLConnection sendAsyncRequest:request
//                  completionHandler:^(NSURLResponse *response,
//                                      NSData *data,
//                                      NSError *error) {
//    NSArray *turnServers = [NSArray array];
//    if (error) {
//      NSLog(@"Unable to get TURN server.");
//      completionHandler(turnServers);
//      return;
//    }
//    NSDictionary *dict = [NSDictionary dictionaryWithJSONData:data];
//    turnServers = [RTCICEServer serversFromCEODJSONDictionary:dict];
//    completionHandler(turnServers);
//  }];
//}




//#pragma mark - Room server methods
//
//- (void)registerWithRoomServerForRoomId:(NSString *)roomId
//    completionHandler:(void (^)(ARDRegisterResponse *))completionHandler {
//  NSString *urlString =
//      [NSString stringWithFormat:kARDRoomServerRegisterFormat, self.serverHostUrl, roomId];
//  NSURL *roomURL = [NSURL URLWithString:urlString];
//  NSLog(@"Registering with room server.");
//  __weak ARDAppClient *weakSelf = self;
//  [NSURLConnection sendAsyncPostToURL:roomURL
//                             withData:nil
//                    completionHandler:^(BOOL succeeded, NSData *data) {
//    ARDAppClient *strongSelf = weakSelf;
//    if (!succeeded) {
//      NSError *error = [self roomServerNetworkError];
//      [strongSelf.delegate appClient:strongSelf didError:error];
//      completionHandler(nil);
//      return;
//    }
//    ARDRegisterResponse *response =
//        [ARDRegisterResponse responseFromJSONData:data];
//    completionHandler(response);
//  }];
//}
//
//- (void)sendSignalingMessageToRoomServer:(ARDSignalingMessage *)message
//    completionHandler:(void (^)(ARDMessageResponse *))completionHandler {
//  NSData *data = [message JSONData];
//  NSString *urlString =
//      [NSString stringWithFormat:
//          kARDRoomServerMessageFormat, self.serverHostUrl, _roomId, _clientId];
//  NSURL *url = [NSURL URLWithString:urlString];
//  NSLog(@"C->RS POST: %@", message);
//  __weak ARDAppClient *weakSelf = self;
//  [NSURLConnection sendAsyncPostToURL:url
//                             withData:data
//                    completionHandler:^(BOOL succeeded, NSData *data) {
//    ARDAppClient *strongSelf = weakSelf;
//    if (!succeeded) {
//      NSError *error = [self roomServerNetworkError];
//      [strongSelf.delegate appClient:strongSelf didError:error];
//      return;
//    }
//    ARDMessageResponse *response =
//        [ARDMessageResponse responseFromJSONData:data];
//    NSError *error = nil;
//    switch (response.result) {
//      case kARDMessageResultTypeSuccess:
//        break;
//      case kARDMessageResultTypeUnknown:
//        error =
//            [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                       code:kARDAppClientErrorUnknown
//                                   userInfo:@{
//          NSLocalizedDescriptionKey: @"Unknown error.",
//        }];
//      case kARDMessageResultTypeInvalidClient:
//        error =
//            [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                       code:kARDAppClientErrorInvalidClient
//                                   userInfo:@{
//          NSLocalizedDescriptionKey: @"Invalid client.",
//        }];
//        break;
//      case kARDMessageResultTypeInvalidRoom:
//        error =
//            [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                       code:kARDAppClientErrorInvalidRoom
//                                   userInfo:@{
//          NSLocalizedDescriptionKey: @"Invalid room.",
//        }];
//        break;
//    };
//    if (error) {
//      [strongSelf.delegate appClient:strongSelf didError:error];
//    }
//    if (completionHandler) {
//      completionHandler(response);
//    }
//  }];
//}
//
//- (void)unregisterWithRoomServer {
//  NSString *urlString =
//      [NSString stringWithFormat:kARDRoomServerByeFormat, self.serverHostUrl, _roomId, _clientId];
//  NSURL *url = [NSURL URLWithString:urlString];
//  NSLog(@"C->RS: BYE");
//    //Make sure to do a POST
//    [NSURLConnection sendAsyncPostToURL:url withData:nil completionHandler:^(BOOL succeeded, NSData *data) {
//        if (succeeded) {
//            NSLog(@"Unregistered from room server.");
//        } else {
//            NSLog(@"Failed to unregister from room server.");
//        }
//    }];
//}
//
//- (NSError *)roomServerNetworkError {
//  NSError *error =
//      [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                 code:kARDAppClientErrorNetwork
//                             userInfo:@{
//    NSLocalizedDescriptionKey: @"Room server network error",
//  }];
//  return error;
//}
//
//#pragma mark - Collider methods
//
//- (void)registerWithColliderIfReady {
//  if (!self.isRegisteredWithRoomServer) {
//    return;
//  }
//  // Open WebSocket connection.
//  _channel =
//      [[ARDWebSocketChannel alloc] initWithURL:_websocketURL
//                                       restURL:_websocketRestURL
//                                      delegate:self];
//  [_channel registerForRoomId:_roomId clientId:_clientId];
//}
//
//- (void)sendSignalingMessageToCollider:(ARDSignalingMessage *)message {
//  NSData *data = [message JSONData];
//  [_channel sendData:data];
//}

#pragma mark - Defaults

- (RTCMediaConstraints *)defaultMediaStreamConstraints {
  RTCMediaConstraints* constraints =
      [[RTCMediaConstraints alloc]
          initWithMandatoryConstraints:nil
                   optionalConstraints:nil];
  return constraints;
}

- (RTCMediaConstraints *)defaultAnswerConstraints {
  return [self defaultOfferConstraints];
}

- (RTCMediaConstraints *)defaultOfferConstraints {
    NSArray *mandatoryConstraints;
    
    mandatoryConstraints= @[
                            [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"],
                            [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:@"true"]
                            ];
    
  RTCMediaConstraints* constraints =
      [[RTCMediaConstraints alloc]
          initWithMandatoryConstraints:mandatoryConstraints
                   optionalConstraints:nil];
  return constraints;
}

- (RTCMediaConstraints *)defaultPeerConnectionConstraints {
  NSArray *optionalConstraints = @[
      [[RTCPair alloc] initWithKey:@"DtlsSrtpKeyAgreement" value:@"true"]
  ];
  RTCMediaConstraints* constraints =
      [[RTCMediaConstraints alloc]
          initWithMandatoryConstraints:nil
                   optionalConstraints:optionalConstraints];
  return constraints;
}

- (RTCICEServer *)defaultSTUNServer {
  NSURL *defaultSTUNServerURL = [NSURL URLWithString:kARDDefaultSTUNServerUrl];
  return [[RTCICEServer alloc] initWithURI:defaultSTUNServerURL
                                  username:@""
                                  password:@""];
}

@end
