//
//  Utility.h
//  LANAudioMonitor
//
//  Created by BingHuan Wu on 8/14/15.
//  Copyright (c) 2015 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Utility : NSObject

+ (NSDictionary *)getIPAddresses;
//+ (void) broadcastLANWithMessage:(NSString*)message;
//+ (void) sendMessage:(NSString*)message toHost:(NSString*)host;
//+ (void) listenLAN;

+ (void)showAlertWithTitle: (NSString*)title andMessage: (NSString*)message;
+ (BOOL)checkForLAN;
+ (NSString *)getCurrentWifiHotSpotName;
@end
