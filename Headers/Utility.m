//
//  Utility.m
//  LANAudioMonitor
//
//  Created by BingHuan Wu on 8/14/15.
//  Copyright (c) 2015 Gynoii. All rights reserved.
//

#include <ifaddrs.h>
#include <net/if.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#define TIMEOUT 2000 // LAN Broadcast timeout

unsigned int getmsec() {
    struct timeval tv = {0};
    unsigned int msec = 0;
    
    gettimeofday(&tv, NULL);
    msec = (int)((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
    return msec;
}

#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IOS_VPN         @"utun0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"


#import <Foundation/Foundation.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "Utility.h"

@implementation Utility

#pragma mark Network related
+ (NSDictionary *)getIPAddresses
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // Retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type = nil;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}

+ (void) sendMessage:(NSString*)message toHost:(NSString*)host
{
    dispatch_async(dispatch_get_main_queue(), ^{
        int sock;
        if( (sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        {
            return;
        }
        
        int keepAlive = 1;
        if( setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, &keepAlive, sizeof(keepAlive)) != 0 )
        {
            close(sock);
            return;
        }
        
        struct timeval recv_timeout;
        recv_timeout.tv_sec=0;
        recv_timeout.tv_usec=100;
        
        int result = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&recv_timeout, sizeof(recv_timeout));
        if( result !=0 )
        {
            return;
        }
        
        const char *ip = [host UTF8String];
        char * msg = "TEST";
        if (message) {
            msg = (char*)[message UTF8String];
        }
        
        // Setup interface
        struct sockaddr_in si;
        si.sin_family = AF_INET;
        si.sin_port   = htons( 5002 );
        inet_aton( ip, (struct in_addr*)&si.sin_addr.s_addr );
        
        // Send data
        long ret = sendto(sock, msg, strlen(msg), 0, (struct sockaddr*) &si, sizeof(si));
        NSLog(@"sent : %ld",ret);
    });
}

+ (void) broadcastLANWithMessage:(NSString*)message
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Setup socket
        int sock;
        if( (sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        {
            //perror("socket : ");
            return;
        }
        
        int broadcast = 1;
        if( setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) != 0 )
        {
            //perror("setsockopt : ");
            close(sock);
            return;
        }
        struct timeval recv_timeout;
        recv_timeout.tv_sec=0;
        recv_timeout.tv_usec=100;
        
        int result = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&recv_timeout, sizeof(recv_timeout));
        if( result !=0 )
        {
            return;
        }
        
        char *ip = "255.255.255.255";
        char * msg = "TEST";
        if (message) {
            msg = (char*)[message UTF8String];
        }
        
        // Setup interface
        struct sockaddr_in si;
        si.sin_family = AF_INET;
        si.sin_port   = htons( 5002 );
        inet_aton( ip, (struct in_addr*)&si.sin_addr.s_addr );
        
        // Send data
        long ret = sendto(sock, msg, strlen(msg), 0, (struct sockaddr*) &si, sizeof(si));
        NSLog(@"Broadcast sent : %ld",ret);
        
#ifdef DEBUG
        printf("Broadcast LAN\n");
#endif
        
    });
}

+ (void) listenLAN
{
    NSDictionary *ip = [Utility getIPAddresses];
    NSString* myLANIP;
    
    if ([ip objectForKey:@"en0/ipv4"]) {
        myLANIP = [ip objectForKey:@"en0/ipv4"];
    }
    else
    {
        NSLog(@"Not connected to LAN");
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Setup socket
        int sock;
        if( (sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        {
            return;
        }
        
        int keepAlive = 1;
        if( setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, &keepAlive, sizeof(keepAlive)) != 0 )
        {
            close(sock);
            return;
        }
        struct timeval recv_timeout;
        recv_timeout.tv_sec=0;
        recv_timeout.tv_usec=100;
        
        int result = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&recv_timeout, sizeof(recv_timeout));
        if( result !=0 )
        {
            return;
        }
        
        struct sockaddr_in si;
        si.sin_family = AF_INET;
        si.sin_port   = htons( 5002 );
        si.sin_addr.s_addr = INADDR_ANY;
        
        // Bind socket with interface
        bind(sock, (struct sockaddr *)&si, sizeof(struct sockaddr));
        
        // Variables for data receive
        struct sockaddr_in fromadd;
        int recvStringLen = 0;
        unsigned char recvString[10000+1]={0};
        socklen_t addrsize = sizeof(struct sockaddr_in);
        
#ifdef DEBUG
        NSLog(@"Listening LAN (%@) ...\n",myLANIP);
#endif
        
        unsigned int Start = getmsec();
        
        for(;;){
            
            if ((recvStringLen = (int)recvfrom(sock, recvString, 10000, 0, (struct sockaddr*)&fromadd, &addrsize))  > 0){
                char display[16] = {0};
                inet_ntop(AF_INET, &fromadd.sin_addr.s_addr, display, sizeof display);
                
                NSString *IPAddress = [NSString stringWithCString:display encoding:NSASCIIStringEncoding];
                NSString *Data = [NSString stringWithCString:(const char *)recvString encoding:NSASCIIStringEncoding];
                
                if ([IPAddress isEqualToString:myLANIP]) {
//                    NSLog(@"Received packet from myself\n%@",Data);
                }
                else {
//                    NSLog(@"Received from %@\n%@",IPAddress,Data);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"LAN_CAPTURED" object:Data];
                    });
                }
                
                memset(&recvString[0], 0, sizeof(recvString));
            }
            
//            else {
//                if(getmsec()-Start > TIMEOUT) {
//#ifdef DEBUG
//                    printf("\nTime up\n");
//#endif
//                    close(sock);
//                    break;
//                }
//            }
        }
    });
}

#pragma mark UIAlert
+ (void)showAlertWithTitle: (NSString*)title andMessage: (NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alert show];
    });
}

#pragma mark Check if in LAN
+ (BOOL)checkForLAN {
    NSDictionary *ip = [Utility getIPAddresses];
    if ([ip objectForKey:@"en0/ipv4"]) {
        NSString *wifi = [Utility getCurrentWifiHotSpotName];
        NSLog(@"wifi name = %@",wifi);
        if (nil != wifi) {
            return YES;
        }
    }
    return NO;
}

+ (NSString *)getCurrentWifiHotSpotName {
    NSString *wifiName = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
#ifdef DEBUG
        NSLog(@"info:%@",info);
#endif
        if (info[@"SSID"]) {
            wifiName = info[@"SSID"];
        }
    }
    return wifiName;
}

@end

