//
//  AccountViewController.m
//  AppRTC
//
//  Created by BingHuan Wu on 10/30/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "AccountViewController.h"

@interface AccountViewController ()
{
    IBOutlet UIButton *logoutButton;
    IBOutlet UILabel *username;
    IBOutlet UILabel *email;
    
    IBOutlet UILabel *usernameTitle;
    IBOutlet UILabel *emailTitle;
}

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Localize
    [usernameTitle setText:NSLocalizedString(@"User Name", nil)];
    [emailTitle setText:NSLocalizedString(@"Email Address", nil)];
    [logoutButton setTitle:NSLocalizedString(@"Logout", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"username"]) {
        [username setText:[defaults objectForKey:@"username"]];
    }
    if ([defaults objectForKey:@"email"]) {
        [email setText:[defaults objectForKey:@"email"]];
    }
}

- (IBAction)logout:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![[defaults objectForKey:@"remember"] boolValue]) {
        [defaults setObject:@"" forKey:@"username"];
        [defaults setObject:@"" forKey:@"email"];
        [defaults synchronize];
    }
    
    UINavigationController *vc = (UINavigationController *)(self.presentingViewController);
    [vc popToRootViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
