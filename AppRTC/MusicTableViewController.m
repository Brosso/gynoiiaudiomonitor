//
//  MusicTableViewController.m
//  AppRTC
//
//  Created by BingHuan Wu on 10/26/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "MusicTableViewController.h"
#import "Utility.h"
#import "SocketManager.h"

@interface MusicTableViewController ()
{
    NSMutableArray *playList;
}

@end

@implementation MusicTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(showLibrary)];
    
    self.navigationItem.rightBarButtonItem = addButton; //self.editButtonItem;
    self.title = NSLocalizedString(@"Music List", nil);
    
    // Default tableview cell
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"playListCell"];
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    
    // Read data and load table view
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"playlist.plist"];
    NSString *bundle = [[NSBundle mainBundle] pathForResource:@"playlist" ofType:@"plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path])
    {
        [fileManager copyItemAtPath:bundle toPath: path error:&error];
    }
    
    playList = [[NSMutableArray alloc] initWithContentsOfFile:path]; // array of dictionary with name and url
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.hidden = NO;
}

- (void)showLibrary {
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeMusic];
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = NO;
    mediaPicker.prompt = NSLocalizedString(@"Select Your Favourite Song!", nil);
    [mediaPicker loadView];
    [self.navigationController presentViewController:mediaPicker animated:YES completion:nil];
}

- (void)sendJsonCommand:(NSDictionary*)jsonDict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:0 error:&error];
    
    NSString *jsonString = nil;
    if (!jsonData) {
        //        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    SocketManager *manager = [SocketManager sharedInstance];
    
    if (nil != [manager getLastPeer]) {
        [manager sendMessage:jsonString toHost:[manager getLastPeer]];
    }
}

#pragma mark MPMediaPickerController delegate

- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{
    [mediaPicker dismissViewControllerAnimated:NO completion:nil];
    
    // Retrive media item
    MPMediaItem *mediaItem = [[mediaItemCollection items] objectAtIndex:0];
    NSString* soundName = [mediaItem valueForProperty:MPMediaItemPropertyTitle];
    NSURL *soundUrl = [mediaItem valueForProperty:MPMediaItemPropertyAssetURL];
    
//    AVURLAsset *songAsset = [AVURLAsset URLAssetWithURL: soundUrl options:nil];
    
//    NSLog(@"soundName = %@",soundName);
//    NSLog(@"soundUrl = %@",[soundUrl absoluteString]);
    
    if (nil == playList || [playList count] >= 8) {
        NSLog(@"bad play list or count >= 8");
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Exceeds the limit of 5 custom music. Please remove other music by swiping its cell from the playlist", nil)];
        return;
    }
    
    for (NSDictionary *dict in playList) {
        if ([[dict objectForKey:@"url"] isEqualToString:[soundUrl absoluteString]]) {
            NSLog(@"duplicate song");
            [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Music already exists in the playlist", nil)];
            return;
        }
    }
    
    NSMutableDictionary *newEntry = [[NSMutableDictionary alloc] init];
    [newEntry setObject:soundName forKey:@"name"];
    [newEntry setObject:[soundUrl absoluteString] forKey:@"url"];
    
    // add to play list
    [playList addObject:newEntry];
    [self.tableView reloadData];
    
    // save back to file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"playlist.plist"];
    
    [playList writeToFile:path atomically:YES];
    
    [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Music saved to playlist. Viewer can now select and play this music remotely.", nil)];
    
    NSMutableDictionary *aJsonDict = [[NSMutableDictionary alloc] init];
    [aJsonDict setObject:@"musicList" forKey:@"type"];
    
    NSMutableArray *finalSongList = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in playList) {
        [finalSongList addObject:[dict objectForKey:@"name"]];
    }
    
    [aJsonDict setObject:finalSongList forKey:@"list"];
    
    [self sendJsonCommand:aJsonDict];
}

- (void) mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker
{
    dispatch_barrier_async(dispatch_get_main_queue(), ^{
        [mediaPicker dismissViewControllerAnimated:NO completion:nil];
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [playList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"playListCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSDictionary *songInfo = [playList objectAtIndex:indexPath.row];
    cell.textLabel.text = [songInfo objectForKey:@"name"];
    
    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:20];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSDictionary *songInfo = [playList objectAtIndex:indexPath.row];
//    
//    NSLog(@"name = %@", [songInfo objectForKey:@"name"]);
//    NSLog(@"url = %@", [songInfo objectForKey:@"url"]);
    
    // delete or preview?
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
        return NO;
    }
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [playList removeObjectAtIndex:indexPath.row];
        
        // save back to file
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:@"playlist.plist"];
        
        [playList writeToFile:path atomically:YES];
        
        [self.tableView reloadData];
    }
}


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
