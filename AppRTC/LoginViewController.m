//
//  LoginViewController.m
//  AppRTC
//
//  Created by BingHuan Wu on 11/4/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "LoginViewController.h"
#import "Utility.h"

@interface LoginViewController ()
{
    IBOutlet UITextField *username;
    IBOutlet UITextField *email;
    IBOutlet UILabel  *rememberLabel;
    IBOutlet UIButton *loginBtn;
    IBOutlet UIButton *checkBtn;
    IBOutlet UISwitch *rememberSwitch;
    IBOutlet UIButton *helpBtn;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	self.title = NSLocalizedString(@"Login", nil);
    // Localize
    [username setPlaceholder:NSLocalizedString(@"User Name", nil)];
    [email setPlaceholder:NSLocalizedString(@"Email Address", nil)];
    [rememberLabel setText:NSLocalizedString(@"Remember me", nil)];
    [loginBtn setTitle:NSLocalizedString(@"Login", nil) forState:UIControlStateNormal];
    [helpBtn setTitle:NSLocalizedString(@"Need help?", nil) forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismisskeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
	
    // Status bar style
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [checkBtn setSelected:[[defaults objectForKey:@"remember"] boolValue]];
    [rememberSwitch setOn:[[defaults objectForKey:@"remember"] boolValue]];
    
    if ([[defaults objectForKey:@"remember"] boolValue]) {
        if ([defaults objectForKey:@"username"]) {
            [username setText:[defaults objectForKey:@"username"]];
        }
        if ([defaults objectForKey:@"email"]) {
            [email setText:[defaults objectForKey:@"email"]];
        }
    }
    
    
    if ([[defaults objectForKey:@"newInstall"] boolValue]) {
        [defaults setObject:[NSNumber numberWithBool:NO] forKey:@"newInstall"];
        [defaults synchronize];
        
        [self performSegueWithIdentifier:@"testSegue" sender:nil];
    }
    
}


- (IBAction)test:(id)sender
{
    // test
    [self performSegueWithIdentifier:@"testSegue" sender:nil];
}

- (IBAction)switchValueChanged:(id)sender
{
    // Behavior is different to UIButton
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![rememberSwitch isOn]) {
        [defaults setObject:[NSNumber numberWithBool:NO] forKey:@"remember"];
        [defaults setObject:@"" forKey:@"username"];
        [defaults setObject:@"" forKey:@"email"];
    }
    else {
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"remember"];
    }
    
    [defaults synchronize];
}

- (IBAction)CheckButtonPressed:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([checkBtn isSelected]) {
        [checkBtn setSelected:NO];
        [defaults setObject:[NSNumber numberWithBool:NO] forKey:@"remember"];
        [defaults setObject:@"" forKey:@"username"];
        [defaults setObject:@"" forKey:@"email"];
    }
    else {
        [checkBtn setSelected:YES];
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"remember"];
    }
    
    [defaults synchronize];
}

- (IBAction)Login:(id)sender
{
    if ([username.text isEqualToString:@""]) {
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Empty username", nil)];
        return;
    }
    if ([email.text isEqualToString:@""]) {
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Empty Email address", nil)];
        return;
    }
    
    if (![self NSStringIsValidEmail:email.text]) {
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Invalid Email format", nil)];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:username.text forKey:@"username"];
    [defaults setObject:email.text forKey:@"email"];
    [defaults synchronize];
    
    [self performSegueWithIdentifier:@"LoginSegue" sender:nil];
}

- (void)dismisskeyboard
{
    [username resignFirstResponder];
    [email resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
