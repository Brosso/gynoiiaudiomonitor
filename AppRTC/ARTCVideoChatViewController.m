//
//  ARTCVideoChatViewController.m
//  AppRTC
//
//  Created by Kelly Chu on 3/7/15.
//  Copyright (c) 2015 ISBX. All rights reserved.
//

#import "ARTCVideoChatViewController.h"
#import "ARTCRoomViewController.h"
#import "TutorialViewController.h"
#import "SocketManager.h"
#import <AVFoundation/AVFoundation.h>

#define SERVER_HOST_URL @"https://apprtc.appspot.com"

@import MediaPlayer;

@implementation ARTCVideoChatViewController
{
    BOOL isInitiator;
    NSMutableArray *songListArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.isZoom = NO;
    [self.audioButton.layer setCornerRadius:20.0f];
    [self.videoButton.layer setCornerRadius:20.0f];
    [self.hangupButton.layer setCornerRadius:20.0f];
    
    //Add Tap to hide/show controls
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleButtonContainer)];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    //Add Double Tap to zoom
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomRemote)];
    [tapGestureRecognizer setNumberOfTapsRequired:2];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    //RTCEAGLVideoViewDelegate provides notifications on video frame dimensions
    [self.remoteView setDelegate:self];
    [self.localView setDelegate:self];
    
    // Empty song list
    songListArray = [[NSMutableArray alloc] init];
    
    // Set UI
    if (isInitiator) { // viewer
		_lockButton.hidden = _musicEditButton.hidden = YES;
		_hangupButton.hidden = NO;
    }
    else { // monitor
        _audioButton.hidden = _musicButton.hidden = _ledButton.hidden = _snapshotButton.hidden = YES;
		_hangupButton2.hidden = NO;
    }
	
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    static BOOL showViewerAlert = YES;
    static BOOL showMonitorAlert = YES;
    
    // UI stuff
    self.backgroundView.hidden = NO;
    [_audioButton setSelected:YES];
    [self setControlEnabled:NO];

    self.navigationController.navigationBar.hidden = YES;
    
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
    // Check last peer
    SocketManager *manager = [SocketManager sharedInstance];
    if (nil == [manager getLastPeer] || [[manager getLastPeer] isEqualToString:@""]) {
        [manager broadcastLANWithMessage:nil];
#ifdef DEBUG
        NSLog(@"ping again~");
#endif
    }
    
#ifdef DEBUG
    NSLog(@"last peer:%@.",[manager getLastPeer]);
#endif

    
    //Display the Local View full screen while connecting to Room
    [self.localViewBottomConstraint setConstant:0.0f];
    [self.localViewRightConstraint setConstant:0.0f];
    [self.localViewHeightConstraint setConstant:self.view.frame.size.height];
    [self.localViewWidthConstraint setConstant:self.view.frame.size.width];

//    [self.footerViewBottomConstraint setConstant:0.0f];
    
    if (isInitiator && showViewerAlert) { // viewer
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"This device is a viewer now!\n\nNext, login with the same account on the second phone, and set it as a monitor.", nil)];
        showViewerAlert = NO;
    }
    else if (!isInitiator && showMonitorAlert) { // monitor
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"This device is a monitor now!\n\nNext, login with the same account on the second phone, and set it as a viewer.", nil)];
        showMonitorAlert = NO;
    }
    
    // Connect to the room
    [self disconnect];
    self.client = [[ARDAppClient alloc] initWithDelegate:self];
    [self.client setInitiator:isInitiator];

    [self.client setServerHostUrl:SERVER_HOST_URL];
    [self.client connectToRoomWithId:self.roomName options:nil];
    
    
//    [self.urlLabel setText:self.roomUrl];
    
    // Disable app idle timer
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    //Getting Orientation change
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged:)
                                                 name:@"UIDeviceOrientationDidChangeNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resetConnection)
                                                 name:@"MaximumRetry"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setSongList:)
                                                 name:@"GOT_MUSIC_LIST"
                                               object:nil];
    
    // Audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setMode:AVAudioSessionModeVideoChat error:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (isInitiator) {
        [self.client stopMusic];
    }
    
    [self disconnect];
    
    // Enable app idle timer
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"TutorialSegue"]) {
        TutorialViewController *tVC = segue.destinationViewController;
        [tVC setContent:isInitiator];
    }
}

- (void)dealloc {
//    NSLog(@"VideoChatView dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MaximumRetry" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GOT_MUSIC_LIST" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)orientationChanged:(NSNotification *)notification{
//    [self videoView:self.localView didChangeVideoSize:self.localVideoSize];
//    [self videoView:self.remoteView didChangeVideoSize:self.remoteVideoSize];
    
    // rotate UI angle only
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    CGFloat angle = 0.0f;
    
    switch (orientation) {
        case UIDeviceOrientationLandscapeLeft:
        {
            angle = M_PI/2;
            break;
        }
        case UIDeviceOrientationLandscapeRight:
        {
            angle = -M_PI/2;
            break;
        }
        case UIDeviceOrientationPortrait:
        {
            angle = 0;
            break;
        }
        case UIDeviceOrientationPortraitUpsideDown:
        {
            angle = M_PI;
            break;
        }
        default:
        {
            angle = 0;
            break;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4f animations:^{
            CGAffineTransform transform = CGAffineTransformMakeRotation(angle);
            _audioButton.transform = _videoButton.transform = _hangupButton.transform = _musicButton.transform = _snapshotButton.transform = _ledButton.transform = _lockButton.transform = _musicEditButton.transform = _hangupButton2.transform = _settingButton.transform = _tutorialButton.transform = _unlockButton.transform = transform;
            
            [self.view layoutIfNeeded];
        }];
    });

    
}

- (void)setSongList:(NSNotification *)notification
{
    if (nil != [notification object]) {
        songListArray = [notification object];
    }
}


- (void)setInitiator:(BOOL)isInitiatorr
{
//    [self.client setInitiator:isInitiator];
    isInitiator = isInitiatorr;
}

- (void)setRoomName:(NSString *)roomName {
    _roomName = roomName;
    self.roomUrl = [NSString stringWithFormat:@"%@/r/%@", SERVER_HOST_URL, roomName];
}

- (void)disconnect {
    if (self.client) {
        if (self.localVideoTrack) [self.localVideoTrack removeRenderer:self.localView];
        if (self.remoteVideoTrack) [self.remoteVideoTrack removeRenderer:self.remoteView];
        self.localVideoTrack = nil;
        [self.localView renderFrame:nil];
        self.remoteVideoTrack = nil;
        [self.remoteView renderFrame:nil];
        [self.client disconnect];
    }
}

- (void)remoteDisconnected {
    if (self.remoteVideoTrack) [self.remoteVideoTrack removeRenderer:self.remoteView];
    self.remoteVideoTrack = nil;
    [self.remoteView renderFrame:nil];
    [self videoView:self.localView didChangeVideoSize:self.localVideoSize];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.backgroundView.hidden = NO;
        [self setControlEnabled:NO];
    });
}

- (void)toggleButtonContainer {
    [UIView animateWithDuration:0.3f animations:^{
        if (0.0f == self.buttonContainerView.alpha) {
            [self.buttonContainerView setAlpha:1.0f];
            [_settingButton setAlpha:1.0f];
            [_tutorialButton setAlpha:1.0f];
        } else {
            [self.buttonContainerView setAlpha:0.0f];
            [_settingButton setAlpha:0.0f];
            [_tutorialButton setAlpha:0.0f];
        }
        [self.view layoutIfNeeded];
    }];
}

- (void)zoomRemote {
    //Toggle Aspect Fill or Fit
    self.isZoom = !self.isZoom;
    [self videoView:self.remoteView didChangeVideoSize:self.remoteVideoSize];
}

- (IBAction)audioButtonPressed:(id)sender { // two way audio
    [self.client toggleAudioTrack];
    
    [_audioButton setSelected:!_audioButton.selected];
    
}

- (IBAction)videoButtonPressed:(id)sender { // front back camera
    [self.client toggleVideoTrack];
}

- (IBAction)hangupButtonPressed:(id)sender {
    
    [self disconnect];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ledButtonPressed:(id)sender {
    [self.client toggleLEDOn];
    
    static BOOL ledState = YES;
    
    if (ledState) {
        [_ledButton setImage:[UIImage imageNamed:@"ledOn"] forState:UIControlStateNormal];
    }
    else {
        [_ledButton setImage:[UIImage imageNamed:@"ledOff"] forState:UIControlStateNormal];
    }
    
    ledState = !ledState;
}

- (IBAction)songListPressed:(id)sender {
//    NSLog(@"songListPressed");
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Select Music", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles: nil];
        alert.delegate = self;
        
        for( NSString *title in songListArray)  {
            [alert addButtonWithTitle:title];
        }
        
        [alert show];
        
        [_musicButton setImage:[UIImage imageNamed:@"music_active"] forState:UIControlStateNormal];
    });
}

- (IBAction)lockPressed:(id)sender {
    _blockerView.hidden = NO;
}

- (IBAction)unlockPressed:(id)sender {
    _blockerView.hidden = YES;
}

- (IBAction)snapshot:(id)sender {
    
    UIGraphicsBeginImageContextWithOptions(self.remoteView.bounds.size, YES, 0);
    [self.remoteView drawViewHierarchyInRect:self.remoteView.bounds afterScreenUpdates:NO];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (image) {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        [Utility showAlertWithTitle:NSLocalizedString(@"Saved to Photos", nil) andMessage:@""];
    }
    else {
        [Utility showAlertWithTitle:NSLocalizedString(@"Fail to snapshot", nil) andMessage:@""];
    }
}

- (IBAction)infoPressed:(id)sender
{
    [self performSegueWithIdentifier:@"TutorialSegue" sender:nil];
}




//- (void)getSongList {
//    MPMediaQuery *everything = [[MPMediaQuery alloc] init];
//    
//    NSLog(@"Logging items from a generic query...");
//    NSArray *itemsFromGenericQuery = [everything items];
//    NSMutableArray *songTitleArray = [[NSMutableArray alloc] init];
//    for (MPMediaItem *song in itemsFromGenericQuery) {
//        NSString *songTitle = [song valueForProperty: MPMediaItemPropertyTitle];
////        NSLog (@"%@", songTitle);
//        [songTitleArray addObject:songTitle];
//    }
//    
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:songTitleArray options:NSJSONWritingPrettyPrinted error:nil];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    
//    NSLog(@"jsonString is:\n%@",jsonString);
//    
//    //-----
//    MPMusicPlayerController* appMusicPlayer =
//    [MPMusicPlayerController applicationMusicPlayer];
//    
//    [appMusicPlayer setShuffleMode: MPMusicShuffleModeOff];
//    [appMusicPlayer setRepeatMode: MPMusicRepeatModeNone];
//    
//    
//    //-----
//    MPMediaPropertyPredicate *artistNamePredicate =
//    [MPMediaPropertyPredicate predicateWithValue: [songTitleArray lastObject]
//                                     forProperty: MPMediaItemPropertyTitle];
//    
//    MPMediaQuery *myArtistQuery = [[MPMediaQuery alloc] init];
//    [myArtistQuery addFilterPredicate: artistNamePredicate];
//    
//    NSArray *itemsFromArtistQuery = [myArtistQuery items];
//    
//    //----
//    
//    [appMusicPlayer setQueueWithQuery:myArtistQuery];
//    [appMusicPlayer play];
//    
//}

- (void)resetConnection {
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSLog(@"!!!resetConnection!!!");
        [self.navigationController popViewControllerAnimated:YES];
    });

//    dispatch_async(dispatch_get_main_queue(), ^{
//        ARTCRoomViewController *vc = [self.navigationController.viewControllers firstObject];
//        [vc.tableView setHidden:YES];
//        
//        [CATransaction begin];
//        [CATransaction setCompletionBlock:^{
//            // handle completion here
//            if (isInitiator) { // viewer
//                [vc setViewer];
//            }
//            else { // camera
//                [vc setCamera];
//            }
//        }];
//        
//        [self.navigationController popToRootViewControllerAnimated:NO];
//        
//        [CATransaction commit];
//    });

}

- (void)setControlEnabled:(BOOL)enabled
{
    _audioButton.enabled = _videoButton.enabled = _hangupButton.enabled = _musicButton.enabled = _snapshotButton.enabled = _ledButton.enabled = _lockButton.enabled = _musicEditButton.enabled = _hangupButton2.enabled = enabled;
}

#pragma mark - ARDAppClientDelegate

- (void)appClient:(ARDAppClient *)client didChangeState:(ARDAppClientState)state {
    switch (state) {
        case kARDAppClientStateConnected:
//            NSLog(@"Client connected.");
            break;
        case kARDAppClientStateConnecting:
//            NSLog(@"Client connecting.");
            break;
        case kARDAppClientStateDisconnected:
//            NSLog(@"Client disconnected.");
            [self remoteDisconnected];
            break;
    }
}

- (void)appClient:(ARDAppClient *)client didReceiveLocalVideoTrack:(RTCVideoTrack *)localVideoTrack {
    if (self.localVideoTrack) {
        [self.localVideoTrack removeRenderer:self.localView];
        self.localVideoTrack = nil;
        [self.localView renderFrame:nil];
    }
    self.localVideoTrack = localVideoTrack;
    [self.localVideoTrack addRenderer:self.localView];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.backgroundView.hidden = YES;
        [self setControlEnabled:YES];
        
        if (isInitiator) { // viewer
            self.localView.hidden = YES;
        }
        else { // camera
            self.localView.hidden = NO;
        }
    });
}

- (void)appClient:(ARDAppClient *)client didReceiveRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack {
    self.remoteVideoTrack = remoteVideoTrack;
    [self.remoteVideoTrack addRenderer:self.remoteView];
    
    self.backgroundView.hidden = YES;
    [self setControlEnabled:YES];

    [UIView animateWithDuration:0.4f animations:^{
        
        [self.localView setHidden:YES];
        
        [self.localViewBottomConstraint setConstant:28.0f];
        [self.localViewRightConstraint setConstant:28.0f];
        [self.localViewHeightConstraint setConstant:self.view.frame.size.height/4.0f];
        [self.localViewWidthConstraint setConstant:self.view.frame.size.width/4.0f];
        [self.footerViewBottomConstraint setConstant:-80.0f];
        [self.view layoutIfNeeded];
    }];
}

- (void)appClient:(ARDAppClient *)client didError:(NSError *)error {
//    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Old style alert"
//                                                        message:[NSString stringWithFormat:@"%@", error]
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//    [alertView show];
    NSLog(@"Old style alert: %@", [NSString stringWithFormat:@"%@", error]);
    [self disconnect];
}

- (void)pauseRemoteVideo
{
    [self remoteDisconnected];
}

- (void)resumeRemoteVideo
{
    if (self.remoteVideoTrack) {
        [self.remoteVideoTrack addRenderer:self.remoteView];
    }
}

#pragma mark - RTCEAGLVideoViewDelegate

- (void)videoView:(RTCEAGLVideoView *)videoView didChangeVideoSize:(CGSize)size {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        [UIView animateWithDuration:0.4f animations:^{
            CGFloat containerWidth = self.view.frame.size.width;
            CGFloat containerHeight = self.view.frame.size.height;
            CGSize defaultAspectRatio = CGSizeMake(4, 3);
//            CGFloat angle = 0.0f;
            
//            switch (orientation) {
//                case UIDeviceOrientationLandscapeLeft:
//                {
//                    angle = -M_PI/2;
//                    break;
//                }
//                case UIDeviceOrientationLandscapeRight:
//                {
//                    angle = M_PI/2;
//                    break;
//                }
//                case UIDeviceOrientationPortrait:
//                {
//                    angle = 0;
//                    break;
//                }
//                case UIDeviceOrientationPortraitUpsideDown:
//                {
//                    angle = 0;
//                    break;
//                }
//                default:
//                {
//                    angle = 0;
//                    break;
//                }
//            }
            
            
            if (videoView == self.localView) {
                //Resize the Local View depending if it is full screen or thumbnail
                self.localVideoSize = size;
                CGSize aspectRatio = CGSizeEqualToSize(size, CGSizeZero) ? defaultAspectRatio : size;
                CGRect videoRect = self.view.bounds;
                if (self.remoteVideoTrack) {
                    videoRect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width/4.0f, self.view.frame.size.height/4.0f);
                    if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
                        videoRect = CGRectMake(0.0f, 0.0f, self.view.frame.size.height/4.0f, self.view.frame.size.width/4.0f);
                    }
                }
                CGRect videoFrame = AVMakeRectWithAspectRatioInsideRect(aspectRatio, videoRect);
                
                //Resize the localView accordingly
                [self.localViewWidthConstraint setConstant:videoFrame.size.width];
                [self.localViewHeightConstraint setConstant:videoFrame.size.height];
                if (self.remoteVideoTrack) {
                    [self.localViewBottomConstraint setConstant:28.0f]; //bottom right corner
                    [self.localViewRightConstraint setConstant:28.0f];
                } else {
                    [self.localViewBottomConstraint setConstant:containerHeight/2.0f - videoFrame.size.height/2.0f]; //center
                    [self.localViewRightConstraint setConstant:containerWidth/2.0f - videoFrame.size.width/2.0f]; //center
                }
            } else if (videoView == self.remoteView) {
                //Resize Remote View
                self.remoteVideoSize = size;
                CGSize aspectRatio = CGSizeEqualToSize(size, CGSizeZero) ? defaultAspectRatio : size;
                CGRect videoRect = self.view.bounds;
                CGRect videoFrame = AVMakeRectWithAspectRatioInsideRect(aspectRatio, videoRect);
                if (self.isZoom) {
                    //Set Aspect Fill
                    CGFloat scale = MAX(containerWidth/videoFrame.size.width, containerHeight/videoFrame.size.height);
                    videoFrame.size.width *= scale;
                    videoFrame.size.height *= scale;
                }
                [self.remoteViewTopConstraint setConstant:containerHeight/2.0f - videoFrame.size.height/2.0f];
                [self.remoteViewBottomConstraint setConstant:containerHeight/2.0f - videoFrame.size.height/2.0f];
                [self.remoteViewLeftConstraint setConstant:containerWidth/2.0f - videoFrame.size.width/2.0f]; //center
                [self.remoteViewRightConstraint setConstant:containerWidth/2.0f - videoFrame.size.width/2.0f]; //center
                
            }
            
            [self.view layoutIfNeeded];
        }];
 
    });
}

#pragma mark UIAlertView delegete - will deprecate soon?
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    NSLog(@"buttonIndex = %d",buttonIndex);
    if (buttonIndex > 0) {
        NSString *songTitle = [songListArray objectAtIndex:(buttonIndex-1)];
//        NSLog(@"songTitle = %@",songTitle);
        [self.client playMusic:songTitle];
    }
    else {
        // cancel button
//        NSLog(@"cancel / stop playing");
        [self.client stopMusic];
    }
    
    [_musicButton setImage:[UIImage imageNamed:@"music"] forState:UIControlStateNormal];
}


@end
