//
//  ARTCRoomViewController.m
//  AppRTC
//
//  Created by Kelly Chu on 3/7/15.
//  Copyright (c) 2015 ISBX. All rights reserved.
//

#import "ARTCRoomViewController.h"
#import "ARTCVideoChatViewController.h"


@implementation ARTCRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.hidden = YES;
    
    NSLog(@"SSID: %@",[Utility getCurrentWifiHotSpotName]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//- (BOOL)prefersStatusBarHidden {
//    return NO;
//}

- (void)setViewer
{
    if (![Utility checkForLAN]) {
        [Utility showAlertWithTitle:@"" andMessage:@"Not in LAN"];
        return;
    }
    
    [self performSegueWithIdentifier:@"ARTCVideoChatViewController" sender:[NSNumber numberWithBool:YES]];
}

- (void)setCamera
{
    if (![Utility checkForLAN]) {
        [Utility showAlertWithTitle:@"" andMessage:@"Not in LAN"];
        return;
    }
    
    [self performSegueWithIdentifier:@"ARTCVideoChatViewController" sender:[NSNumber numberWithBool:NO]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        ARTCRoomTextInputViewCell *cell = (ARTCRoomTextInputViewCell *)[tableView dequeueReusableCellWithIdentifier:@"RoomInputCell" forIndexPath:indexPath];
        [cell setDelegate:self];
        
        return cell;
    }
    
    return nil;
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ARTCVideoChatViewController *viewController = (ARTCVideoChatViewController *)[segue destinationViewController];
    
    if ([sender isKindOfClass:[NSString class]]) {
        [viewController setRoomName:sender];
    }
    else if ([sender isKindOfClass:[NSNumber class]]) {
        if (YES == [sender boolValue]) {
            // isInitiator
            [viewController setInitiator:YES];
        }
        else if (NO == [sender boolValue]) {
            [viewController setInitiator:NO];
        }
    }

}

#pragma mark - ARTCRoomTextInputViewCellDelegate Methods

- (void)roomTextInputViewCell:(ARTCRoomTextInputViewCell *)cell shouldJoinRoom:(NSString *)room {
    [self performSegueWithIdentifier:@"ARTCVideoChatViewController" sender:room];
}

@end
