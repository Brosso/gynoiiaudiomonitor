//
//  SetupGuideViewController.m
//  AppRTC
//
//  Created by BingHuan Wu on 11/16/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "SetupGuideViewController.h"

@interface SetupGuideViewController ()
{
    NSArray *viewcontrollers;
}

@end

@implementation SetupGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.dataSource = self;
    self.delegate = self;
    
    UIViewController *p1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Guide1"];
    UIViewController *p2 = [self.storyboard instantiateViewControllerWithIdentifier:@"Guide2"];
//    UIViewController *p3 = [self.storyboard instantiateViewControllerWithIdentifier:@"Guide3"];
    
//    viewcontrollers = @[p1, p2, p3];
     viewcontrollers = @[p1, p2];
    
    [self setViewControllers:@[p1] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    
    NSUInteger currentIndex = [viewcontrollers indexOfObject:viewController];
    
    --currentIndex;
//    currentIndex = currentIndex % (viewcontrollers.count);
    return ((int)currentIndex < 0)? nil: [viewcontrollers objectAtIndex:currentIndex];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
    NSUInteger currentIndex = [viewcontrollers indexOfObject:viewController];
    
    ++currentIndex;
//    currentIndex = currentIndex % (viewcontrollers.count);
    return ((int)currentIndex > viewcontrollers.count-1)? nil: [viewcontrollers objectAtIndex:currentIndex];;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController // The number of items reflected in the page indicator.
{
    return [viewcontrollers count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController  // The selected item reflected in the page indicator.
{
    return 0;
}

@end
