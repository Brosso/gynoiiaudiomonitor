//
//  AdsController.m
//  AppRTC
//
//  Created by Jintin on 12/16/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "AdsController.h"

@interface AdsController ()
{
	
	__weak IBOutlet UIButton *closeButton;
	
	__weak IBOutlet UIImageView *imageView;
}
@end

@implementation AdsController

NSString *siteUrl;

-(void) viewDidLoad{
	closeButton.hidden = true;
	imageView.hidden = true;
	[self getCountryCode];
}

- (void) getCountryCode {
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://freegeoip.net/json"] cachePolicy:(NSURLRequestUseProtocolCachePolicy) timeoutInterval:5];
	
	__block NSDictionary *json;
	[NSURLConnection sendAsynchronousRequest:request
									   queue:[NSOperationQueue mainQueue]
						   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
							   if (connectionError != nil) {
								   [self nextController];
							   } else {
							   json = [NSJSONSerialization JSONObjectWithData:data
																	  options:0
																		error:nil];
							   NSString *code = [json objectForKey:@"country_code"];
							   [self getAds:code];
							   NSLog(@"Async JSON: %@", [json objectForKey:@"country_code"]);
							   }
						   }];
}

- (void) getAds:(NSString *) code {
	NSLog(@"https://s3-us-west-2.amazonaws.com/gynoii/ads/%@/list.json",code);
	
	NSString *string = [NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/gynoii/ads/%@/list.json",code];
	NSURL *url = [NSURL URLWithString:string];
	
	NSURLRequest *request =
	[[NSURLRequest alloc] initWithURL:url
						  cachePolicy:(NSURLRequestUseProtocolCachePolicy) timeoutInterval:5];
	
	
	__block NSDictionary *json;
	[NSURLConnection sendAsynchronousRequest:request
									   queue:[NSOperationQueue mainQueue]
						   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
							   if (connectionError != nil) {
								   [self nextController];
							   } else {
							   json = [NSJSONSerialization JSONObjectWithData:data
																	  options:0
																		error:nil];
							   //							   {
							   //								   "url":"http://www.google.com",
							   //								   "image":"https://s3-us-west-2.amazonaws.com/gynoii/ads/TW/ads.png"
							   //							   }
							   
							   siteUrl = [json objectForKey:@"url"];
							   NSString *image = [json objectForKey:@"image"];
							   NSLog(@"site %@", siteUrl);
							   NSLog(@"image %@", image);
							   if (image == nil) {
								   [self nextController];
							   } else {
								   
								   NSURL *url = [NSURL URLWithString:image];
								   NSData *nsdata = [NSData dataWithContentsOfURL:url];
								   UIImage *img = [[UIImage alloc] initWithData:nsdata];
								   imageView.image = img;
								   imageView.hidden = false;
								   closeButton.hidden = false;
								   UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(link)];
								   singleTap.numberOfTapsRequired = 1;
								   [imageView setUserInteractionEnabled:YES];
								   [imageView addGestureRecognizer:singleTap];
							   }
							   }
						   }];
}

-(void) nextController {
	
	[self performSegueWithIdentifier:@"ads" sender:self];
}

-(void)link{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:siteUrl]];
}

@end
