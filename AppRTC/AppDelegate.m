//
//  AppDelegate.m
//  AppRTC
//
//  Created by Kelly Chu on 3/7/15.
//  Copyright (c) 2015 ISBX. All rights reserved.
//

#import "AppDelegate.h"
#import "RTCPeerConnectionFactory.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [RTCPeerConnectionFactory initializeSSL];
    
    // Request microphone permission.
    typedef void (^PermissionBlock)(BOOL granted);
    
    PermissionBlock permissionBlock = ^(BOOL granted) {
#ifdef DEBUG
        NSLog(@"Request microphone permission: %@.", (granted) ? @"accessed" : @"denied");
#endif
    };
    
    if ([[AVAudioSession sharedInstance] respondsToSelector:@selector(requestRecordPermission:)] == YES) {
        [[AVAudioSession sharedInstance] performSelector:@selector(requestRecordPermission:) withObject:permissionBlock];
    }
    
    // Request camera permission.
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
#ifdef DEBUG
        NSLog(@"Request camera permission: %@.", (granted) ? @"accessed" : @"denied");
#endif
    }];
    
    // Request album permission
    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        *stop = YES;
    } failureBlock:^(NSError *error) {

    }];
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
#ifdef DEBUG
    NSLog(@"Request album permission: %@.", (status == ALAuthorizationStatusAuthorized) ? @"accessed" : @"denied");
#endif
    
    // User default
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (nil == [defaults objectForKey:@"newInstall"]) {
#ifdef DEBUG
        NSLog(@"New install");
#endif
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"newInstall"];
        [defaults synchronize];
    }
    
    // Status bar style
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    UIPageControl *pageControlAppearance = [UIPageControl appearanceWhenContainedIn:[UIPageViewController class], nil];
    pageControlAppearance.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControlAppearance.currentPageIndicatorTintColor = [UIColor colorWithRed:122.0f/255 green:204.0f/255 blue:153.0f/255 alpha:1.0f]; // GYNOII_GREEN
    pageControlAppearance.backgroundColor = [UIColor whiteColor];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    [RTCPeerConnectionFactory deinitializeSSL];
    
}

@end
