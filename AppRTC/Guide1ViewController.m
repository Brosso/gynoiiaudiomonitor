//
//  Guide1ViewController.m
//  AppRTC
//
//  Created by BingHuan Wu on 11/16/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "Guide1ViewController.h"

@interface Guide1ViewController ()
{
    IBOutlet UILabel *topLabel;
    IBOutlet UILabel *botLabel;
}

@end

@implementation Guide1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [topLabel setText:NSLocalizedString(@"Get two smart devices prepared", nil)];
    [botLabel setText:NSLocalizedString(@"Connect them to the same Wi-Fi and sign in with the same account", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
