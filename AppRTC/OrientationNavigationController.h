//
//  OrientationNavigationController.h
//  AppRTC
//
//  Created by BingHuan Wu on 10/30/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrientationNavigationController : UINavigationController

- (void)setLockPortrait:(BOOL)lock;

@end
