//
//  TutorialViewController.m
//  AppRTC
//
//  Created by BingHuan Wu on 11/6/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()
{
    IBOutlet UIImageView *background;
    IBOutlet UIButton *closeBtn;
    IBOutlet UILabel *settingLabel;
    
    IBOutlet UILabel *lockLabel;
    IBOutlet UILabel *editLabel;
    
    IBOutlet UILabel *talkLabel;
    IBOutlet UILabel *ledLabel;
    IBOutlet UILabel *snapshotLabel;
    IBOutlet UILabel *playLabel;
        
    BOOL isInitiator;
    
    IBOutlet NSLayoutConstraint *settingTop;
    IBOutlet NSLayoutConstraint *settingRight;
    
    IBOutlet NSLayoutConstraint *editRight;
    IBOutlet NSLayoutConstraint *editBot;
    IBOutlet NSLayoutConstraint *lockLeft;
    IBOutlet NSLayoutConstraint *lockBot;
    
    //Copy lock button position
//    IBOutlet NSLayoutConstraint *twowayLeft;
//    IBOutlet NSLayoutConstraint *twowayBot;
    IBOutlet NSLayoutConstraint *ledLeft;
    IBOutlet NSLayoutConstraint *ledBot;
    IBOutlet NSLayoutConstraint *snapshotRight;
    IBOutlet NSLayoutConstraint *snapshotBot;
    IBOutlet NSLayoutConstraint *playRight;
    IBOutlet NSLayoutConstraint *playBot;
    
    
}

@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Localize
    [settingLabel setText:NSLocalizedString(@"Settings", nil)];
    [lockLabel setText:NSLocalizedString(@"Lock Screen", nil)];
    [editLabel setText:NSLocalizedString(@"Edit Song List", nil)];
    
    [talkLabel setText:NSLocalizedString(@"Two-way talk", nil)];
    [ledLabel setText:NSLocalizedString(@"LED switch", nil)];
    [snapshotLabel setText:NSLocalizedString(@"Snapshot", nil)];
    [playLabel setText:NSLocalizedString(@"Music", nil)];
    
    int widthDiff = 220;
    int hightDiff = 20;
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    float aspect = bounds.size.height / bounds.size.width;
    
    // Configure constraint to match different device (iPad)
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [lockLeft setConstant:lockLeft.constant + widthDiff];
        [ledLeft setConstant:ledLeft.constant + widthDiff];
        [playRight setConstant:playRight.constant+ widthDiff];
        [snapshotRight setConstant:snapshotRight.constant + widthDiff];
    }
    
    
    else if (1.5 != aspect) { // new iphone
        [settingTop setConstant:settingTop.constant + hightDiff];
        [lockBot setConstant:lockBot.constant + hightDiff];
        [editBot setConstant:editBot.constant + hightDiff];
        
        [ledBot setConstant:ledBot.constant + hightDiff];
        [snapshotBot setConstant:snapshotBot.constant + hightDiff];
        [playBot setConstant:playBot.constant + hightDiff];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    float aspect = bounds.size.height / bounds.size.width;
    NSString *suffix = (1.5f == aspect || (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))?@"":@"_new";
//    NSLog(@"aspect ratio is %f, suffix is %@.",aspect,suffix);
    
    if (isInitiator) { // Viewer
        NSString *imgName = [NSString stringWithFormat:@"tutorial_viewer%@",suffix];
        [background setImage:[UIImage imageNamed:imgName]];
        lockLabel.hidden = editLabel.hidden = YES;
    }
    else { // Monitor
        NSString *imgName = [NSString stringWithFormat:@"tutorial_monitor%@",suffix];
        [background setImage:[UIImage imageNamed:imgName]];
        talkLabel.hidden = ledLabel.hidden = snapshotLabel.hidden = playLabel.hidden = YES;
        [lockLeft setConstant:0.0f];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setContent:(BOOL)isInitiatorr
{
    isInitiator = isInitiatorr;
}

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
