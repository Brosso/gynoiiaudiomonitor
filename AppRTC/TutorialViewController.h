//
//  TutorialViewController.h
//  AppRTC
//
//  Created by BingHuan Wu on 11/6/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController

- (void)setContent:(BOOL)isInitiator;

@end
