//
//  OrientationNavigationController.m
//  AppRTC
//
//  Created by BingHuan Wu on 10/30/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "OrientationNavigationController.h"

@interface OrientationNavigationController ()
{
    BOOL lockPortrait;
}

@end

@implementation OrientationNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    lockPortrait = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLockPortrait:(BOOL)lock
{
    lockPortrait = lock;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark lock portrait
- (BOOL)shouldAutorotate
{
    if (lockPortrait) {
        return YES;
    }
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if (lockPortrait) {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
