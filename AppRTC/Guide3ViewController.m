//
//  Guide3ViewController.m
//  AppRTC
//
//  Created by BingHuan Wu on 11/16/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "Guide3ViewController.h"

@interface Guide3ViewController ()
{
    IBOutlet UILabel *topLabel;
}

@end

@implementation Guide3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [topLabel setText:NSLocalizedString(@"Start monitoring your baby now!", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
