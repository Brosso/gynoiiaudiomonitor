//
//  RoleSelectionViewController.m
//  AppRTC
//
//  Created by BingHuan Wu on 10/26/15.
//  Copyright © 2015 ISBX. All rights reserved.
//

#import "Utility.h"
#import "RoleSelectionViewController.h"
#import "ARTCVideoChatViewController.h"
#import "SocketManager.h"

@interface RoleSelectionViewController ()
{
    IBOutlet UIButton *monitorBtn;
    IBOutlet UIButton *viewerBtn;
    IBOutlet UILabel  *titleLabel;
	IBOutlet UILabel *remoteName;
}

@end

@implementation RoleSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
//	[self getCountryCode];
    // Localize
    [monitorBtn setTitle:NSLocalizedString(@"Monitor", nil) forState:UIControlStateNormal];
    [viewerBtn setTitle:NSLocalizedString(@"Viewer", nil) forState:UIControlStateNormal];
    [titleLabel setText:NSLocalizedString(@"Set this device as:", nil)];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRemoteName:) name:@"REMOTE_NAME" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchClick:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (IBAction)searchClick:(id)sender {
	remoteName.text = @"";
	SocketManager *manager = [SocketManager sharedInstance];
	[manager stopListenLAN];
	[manager listenLAN];
	[manager broadcastLANWithMessage:nil];
}

- (void)updateRemoteName:(NSNotification *)noti {
	dispatch_async(dispatch_get_main_queue(), ^{
		remoteName.text = noti.object;
	});
	
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
	
//    NSLog(@"SSID: %@",[Utility getCurrentWifiHotSpotName]);
    
    // reset peer
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"peer_IP"];
    [defaults synchronize];
    
    SocketManager *manager = [SocketManager sharedInstance];
    [manager listenLAN];
    [manager broadcastLANWithMessage:nil]; // early ping
}


//- (BOOL)prefersStatusBarHidden {
//    return YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ARTCVideoChatViewController *viewController = (ARTCVideoChatViewController *)[segue destinationViewController];
    
    if ([sender isKindOfClass:[NSString class]]) {
        [viewController setRoomName:sender];
    }
    else if ([sender isKindOfClass:[NSNumber class]]) {
        if (YES == [sender boolValue]) {
            // isInitiator
            [viewController setInitiator:YES];
        }
        else if (NO == [sender boolValue]) {
            [viewController setInitiator:NO];
        }
    }
}


- (IBAction)setViewer:(id)sender
{
    if (![Utility checkForLAN]) {
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Please connect to WiFi network first", nil)];
        return;
    }
    
    [self performSegueWithIdentifier:@"ARTCVideoChatViewController" sender:[NSNumber numberWithBool:YES]];
}

- (IBAction)setCamera:(id)sender
{
    if (![Utility checkForLAN]) {
        [Utility showAlertWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Please connect to WiFi network first", nil)];
        return;
    }
    
    [self performSegueWithIdentifier:@"ARTCVideoChatViewController" sender:[NSNumber numberWithBool:NO]];
}

@end
