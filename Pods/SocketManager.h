//
//  SocketManager.h
//  LANAudioMonitor
//
//  Created by BingHuan Wu on 8/31/15.
//  Copyright (c) 2015 Gynoii. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocketManager : NSObject

+ (id)sharedInstance;
- (void) broadcastLANWithMessage:(NSString*)message;
- (void) sendMessage:(NSString*)message toHost:(NSString*)host;
- (void) listenLAN;
- (void) stopListenLAN;
- (NSString*)getLastPeer;
@end
