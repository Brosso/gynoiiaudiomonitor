//
//  Utility.h
//  LANAudioMonitor
//
//  Created by BingHuan Wu on 8/14/15.
//  Copyright (c) 2015 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Utility : NSObject

+ (NSDictionary *)getIPAddresses;
+ (void)showAlertWithTitle: (NSString*)title andMessage: (NSString*)message;
+ (BOOL)checkForLAN;
+ (NSString *)getCurrentWifiHotSpotName;
@end
