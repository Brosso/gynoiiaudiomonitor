//
//  SocketManager.m
//  LANAudioMonitor
//
//  Created by BingHuan Wu on 8/31/15.
//  Copyright (c) 2015 Gynoii. All rights reserved.
//

#include <ifaddrs.h>
#include <net/if.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#import "SocketManager.h"
#import "Utility.h"

#define PING_MESSAGE    ("GYNOII_PING")
#define PONG_MESSAGE    ("GYNOII_PONG")
#define BROADCAST_IP    ("255.255.255.255")
#define NO_IP           (@"0.0.0.0")
#define LAN_PORT        (5002)
#define LAN_CAPTURED    (@"LAN_CAPTURED")
#define REMOTE_NAME		(@"REMOTE_NAME")


@implementation SocketManager
{
//    NSMutableSet *peerList;
    
    NSString *myLANIP;
    
    int sock_in;
    int sock_out;
    
    BOOL shouldListenLAN;
}

+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

-(id)init
{
    self = [super init];
    
    if (self != nil) {
//        peerList = [[NSMutableSet alloc] init];
        sock_in = 0;
        sock_out = 0;
        shouldListenLAN = NO;
        myLANIP = [[NSString alloc] init];
        
        NSDictionary *ip = [Utility getIPAddresses];
        if ([ip objectForKey:@"en0/ipv4"]) {
            myLANIP = [ip objectForKey:@"en0/ipv4"];
        }
        else {
            myLANIP = NO_IP;
        }
        
//        NSLog(@"INIT: myLANIP is %@",myLANIP);
    }
    return self;
}

#pragma mark socket methods
- (void) broadcastLANWithMessage:(NSString*)message
{
    // Reset sock_out
    if (sock_out) {
        close(sock_out);
        sock_out = 0;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Setup socket
        if( (sock_out = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        {
            NSLog(@"init sock_out fail");
            return;
        }
        
        int broadcast = 1;
        if( setsockopt(sock_out, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) != 0 )
        {
            NSLog(@"setsockopt sock_out fail (SO_BROADCAST)");
            close(sock_out);
            return;
        }
        struct timeval recv_timeout;
        recv_timeout.tv_sec=0;
        recv_timeout.tv_usec=100;
        
        int result = setsockopt(sock_out, SOL_SOCKET, SO_RCVTIMEO, (char *)&recv_timeout, sizeof(recv_timeout));
        if( result !=0 )
        {
            NSLog(@"setsockopt sock_out fail (broadcast SO_RCVTIMEO)");
            close(sock_out);
            return;
        }
        
        char *ip = BROADCAST_IP;
        char * msg = PING_MESSAGE;
        
        // New type default ping
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
        
        [jsonDict setObject:@"ping" forKey:@"type"];
        [jsonDict setObject:[defaults objectForKey:@"username"] forKey:@"id"];
		[jsonDict setObject:[defaults objectForKey:@"email"] forKey:@"email"];
		[jsonDict setObject:[[UIDevice currentDevice] name] forKey:@"name"];
		
		NSString* jsonPING = [self createJSONCommand:jsonDict];
        
        if (nil != jsonPING) {
            msg = (char*)[jsonPING UTF8String];
        }
        
        // With defined message
        if (message) {
            msg = (char*)[message UTF8String];
        }
        
        // Setup interface
        struct sockaddr_in si;
        si.sin_family = AF_INET;
        si.sin_port   = htons( LAN_PORT );
        inet_aton( ip, (struct in_addr*)&si.sin_addr.s_addr );
        
        // Send data
        long ret = sendto(sock_out, msg, strlen(msg), 0, (struct sockaddr*) &si, sizeof(si));
        
#ifdef DEBUG
        if (-1 == ret) {
            NSLog(@"fail to broadcast message %@",message);
        }
        else {
            NSLog(@"I am broadcast message \n%@",message);
        }
#endif
        
    });
}

- (void) sendMessage:(NSString*)message toHost:(NSString*)host
{
    // Reset sock_out
    if (sock_out) {
        close(sock_out);
        sock_out = 0;
    }
    
    // Setup socket
    if( (sock_out = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        NSLog(@"init sock_out fail");
        return;
    }
    
    int keepAlive = 1;
    if( setsockopt(sock_out, SOL_SOCKET, SO_KEEPALIVE, &keepAlive, sizeof(keepAlive)) != 0 )
    {
        NSLog(@"setsockopt sock_out fail (send message SO_KEEPALIVE)");
        close(sock_out);
        return;
    }
    
    struct timeval recv_timeout;
    recv_timeout.tv_sec=0;
    recv_timeout.tv_usec=100;
    
    int result = setsockopt(sock_out, SOL_SOCKET, SO_RCVTIMEO, (char *)&recv_timeout, sizeof(recv_timeout));
    if( result !=0 )
    {
        NSLog(@"setsockopt sock_out fail (send message SO_RCVTIMEO)");
        close(sock_out);
        return;
    }
    
    const char *ip = [host UTF8String];
    if (nil == host) {
        ip = BROADCAST_IP;
    }
    
    char * msg = "";
    if (message) {
        msg = (char*)[message UTF8String];
    }
    
    // Setup interface
    struct sockaddr_in si;
    si.sin_family = AF_INET;
    si.sin_port   = htons( LAN_PORT );
    inet_aton( ip, (struct in_addr*)&si.sin_addr.s_addr );
    
    // Send data
    long ret = sendto(sock_out, msg, strlen(msg), 0, (struct sockaddr*) &si, sizeof(si));
#ifdef DEBUG
    if (-1 == ret) {
        NSLog(@"fail to send message %@ to host %@",message,host);
    }
#endif
}

- (void) listenLAN
{
    if (YES == shouldListenLAN) {
//        NSLog(@"Already listening LAN");
        return;
    }
    
//    // Update peer list
//    [self broadcastLANWithMessage:nil];
    
    // Reset sock_in
    if (sock_in) {
        close(sock_in);
        sock_in = 0;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Setup socket
        if( (sock_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        {
            NSLog(@"init sock_in fail");
            return;
        }
        
        int keepAlive = 1;
        if( setsockopt(sock_in, SOL_SOCKET, SO_KEEPALIVE, &keepAlive, sizeof(keepAlive)) != 0 )
        {
            NSLog(@"setsockopt sock_in fail");
            close(sock_in);
            return;
        }
        struct timeval recv_timeout;
        recv_timeout.tv_sec=0;
        recv_timeout.tv_usec=100;
        
        int result = setsockopt(sock_in, SOL_SOCKET, SO_RCVTIMEO, (char *)&recv_timeout, sizeof(recv_timeout));
        if( result !=0 )
        {
            NSLog(@"setsockopt sock_in fail");
            close(sock_in);
            return;
        }
        
        struct sockaddr_in si;
        si.sin_family = AF_INET;
        si.sin_port   = htons( LAN_PORT );
        si.sin_addr.s_addr = INADDR_ANY;
        
        // Bind socket with interface
        bind(sock_in, (struct sockaddr *)&si, sizeof(struct sockaddr));
        
        // Variables for data receive
        struct sockaddr_in fromadd;
        int recvStringLen = 0;
        unsigned char recvString[10000+1]={0};
        socklen_t addrsize = sizeof(struct sockaddr_in);

        shouldListenLAN = YES;
        
#ifdef DEBUG
        printf("Listening LAN ...\n");
#endif

        while (shouldListenLAN) {
            if ((recvStringLen = (int)recvfrom(sock_in, recvString, 10000, 0, (struct sockaddr*)&fromadd, &addrsize)) > 0) {
                NSDictionary *ip = [Utility getIPAddresses];
//                NSLog(@"recvfrom: myLANIP is %@",[ip objectForKey:@"en0/ipv4"]);
                char display[16] = {0};
                inet_ntop(AF_INET, &fromadd.sin_addr.s_addr, display, sizeof display);
                
                NSString *IPAddress = [NSString stringWithCString:display encoding:NSASCIIStringEncoding];
                NSString *Data = [NSString stringWithCString:(const char *)recvString encoding:NSUTF8StringEncoding]; //NSASCIIStringEncoding
                NSData *stringData = [Data dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:stringData options:0 error:nil];
                
                if ([IPAddress isEqualToString:[ip objectForKey:@"en0/ipv4"]]) {
//                    // Useless
//                    [Utility showAlertWithTitle:@"Received packet from myself" andMessage:Data];
//                    NSLog(@"Received packet from myself: %@",Data);
                }
                else {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                    [peerList addObject:IPAddress];
                    
                    if ([Data isEqualToString:@PING_MESSAGE]) { // old style ping
                        [self broadcastLANWithMessage:@PONG_MESSAGE]; // old style pong
//                        NSLog(@"Answer PONG");
                    }
                    else if (nil != jsonDict) { // new style ping
                        NSString* command = [jsonDict objectForKey:@"type"];
                        NSString *remoteID = [jsonDict objectForKey:@"id"];
						NSString *localID = [defaults objectForKey:@"username"];
						NSString *remoteName = [jsonDict objectForKey:@"name"];
						
						
                        if ([command isEqualToString:@"ping"]) {
                            
                            // Check and store to userdefaults
                            if ([remoteID isEqualToString:localID]) {
								[defaults setObject:IPAddress forKey:@"peer_IP"];
                                [defaults synchronize];
								[[NSNotificationCenter defaultCenter] postNotificationName:REMOTE_NAME object:remoteName];
								
#ifdef DEBUG
                                NSLog(@"[Ping]Stored %@ to user defaults", IPAddress);
#endif
                                
                                NSMutableDictionary *ajsonDict = [[NSMutableDictionary alloc] init];
                                [ajsonDict setObject:@"pong" forKey:@"type"];
                                [ajsonDict setObject:[defaults objectForKey:@"username"] forKey:@"id"];
								[ajsonDict setObject:[defaults objectForKey:@"email"] forKey:@"email"];
								[ajsonDict setObject:[[UIDevice currentDevice] name] forKey:@"name"];
								
								NSString* jsonPONG = [self createJSONCommand:ajsonDict];
                                [self broadcastLANWithMessage:jsonPONG]; // new style pong
    
                            }
                        }
                        
                        else if ([command isEqualToString:@"pong"]) {
                            
                            // Check and store to userdefaults
                            if ([remoteID isEqualToString:localID]) {
								[defaults setObject:IPAddress forKey:@"peer_IP"];
                                [defaults synchronize];
								[[NSNotificationCenter defaultCenter] postNotificationName:REMOTE_NAME object:remoteName];
								
#ifdef DEBUG
                                NSLog(@"[Pong]Stored %@ to user defaults", IPAddress);
#endif
                            }
                        }
                        
                        else {
//                            NSLog(@"ID mismatch or...");
                            [[NSNotificationCenter defaultCenter] postNotificationName:LAN_CAPTURED object:Data];
                        }
                    }
                    else {
                        [[NSNotificationCenter defaultCenter] postNotificationName:LAN_CAPTURED object:Data];
                    }
                }
                
                // Reset buffer
                memset(&recvString[0], 0, sizeof(recvString));
            }
        }
    });
}

- (void) stopListenLAN
{
    if (NO == shouldListenLAN) {
        return;
    }
    
    shouldListenLAN = NO;
    
#ifdef DEBUG
    printf("Stop Listening LAN ...\n");
#endif
	
}

- (NSString*)getLastPeer
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *peer = [defaults objectForKey:@"peer_IP"];
    return peer;
}

- (NSString*)createJSONCommand:(NSDictionary*)jsonDict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:0 error:&error];
    
    NSString *jsonString = nil;
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return jsonString;
}

@end
